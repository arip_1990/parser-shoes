docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

docker-build:
	docker-compose up --build -d

migrate:
	docker-compose exec shoes php yii migrate

# proxy
parse-proxy: proxy-free-list proxy-new-list-1 proxy-new-list-2

proxy-free-list:
	docker-compose exec proxy php yii proxy/get-free-proxy-list

proxy-new-list-1:
	docker-compose exec proxy php yii proxy/get-new-proxies-list

proxy-new-list-2:
	docker-compose exec proxy php yii proxy/get-new-proxies

# zalando make list
zalando-list-men:
	docker-compose exec shoes php yii zalando/make-list-men

zalando-list-women:
	docker-compose exec shoes php yii zalando/make-list-women

zalando-list-garcons:
	docker-compose exec shoes php yii zalando/make-list-garcons

zalando-list-filles:
	docker-compose exec shoes php yii zalando/make-list-filles

# nike make list
nike-list-men:
	docker-compose exec shoes php yii nike/make-list-men

nike-list-women:
	docker-compose exec shoes php yii nike/make-list-women

nike-list-garcons:
	docker-compose exec shoes php yii nike/make-list-garcons

nike-list-filles:
	docker-compose exec shoes php yii nike/make-list-filles

# adidas make list
adidas-list-men:
	docker-compose exec shoes php yii adidas/make-list-men

adidas-list-women:
	docker-compose exec shoes php yii adidas/make-list-women

adidas-list-garcons:
	docker-compose exec shoes php yii adidas/make-list-garcons

adidas-list-filles:
	docker-compose exec shoes php yii adidas/make-list-filles

perm:
	sudo chmod 777 -R shoes/archive shoes/runtime proxy/runtime
