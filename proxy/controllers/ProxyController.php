<?php

namespace console\controllers;

use console\services\ProxyService;
use yii\console\Controller;
use yii\httpclient\Client;

/**
 * Class ProxyController
 * @package console\controllers
 * @property ProxyService $proxyService
 */
class ProxyController extends Controller
{
    private $proxyService;

    const KEY = 'MzIz.XVFTqQ.GG_ALcLUx_cwU2iHNErAdLMKDMg';

    /**
     * ProxyController constructor.
     * @param $id
     * @param $module
     * @param ProxyService $proxyService
     * @param array $config
     */
    public function __construct($id, $module, ProxyService $proxyService, $config = [])
    {
        $this->proxyService = $proxyService;
        parent::__construct($id, $module, $config);
    }


    /**
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    public function actionGetNewProxies()
    {
        //$dataPostProxy = new DataPostProxy(self::KEY);
        $client = new Client(['baseUrl' => 'https://proxy11.com/api/proxy.json?key=' . self::KEY]);
        $response_json = $client->createRequest()
            ->setFormat(Client::FORMAT_JSON)
            ->send();

        $response = $response_json->getData();
        $this->proxyService->newProxies($response);
    }

    /**
     * @throws \Exception
     */
    public function actionGetNewProxiesList()
    {
        $url = 'https://www.proxy-list.download/api/v1/get?type=http&country=US';
        $this->proxyService->newProxiesProxyList($url);
    }

    public function actionGetFreeProxyList()
    {
        $url = 'https://free-proxy-list.net/';
        $this->proxyService->newFreeProxyList($url);
    }

    /**
     * @throws \Exception
     */
    public function actionCheckAvailability()
    {
        $this->proxyService->checkAvailability();
    }

    public function actionCheckProxy() {
        $this->proxyService->checkProxy();
    }

    public function actionTestConnection()
    {
        $url = "https://yandex.ru";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_PROXY, "213.21.18.18:44496");

        $curlResponse = curl_exec($ch);
        echo $curlResponse;
    }
}
