<?php

namespace console\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class Proxy
 * @package console\models
 * @property string $ip
 * @property int $id [int(11) unsigned]
 * @property string $country [varchar(200)]
 * @property string $country_code [varchar(3)]
 * @property bool $google [tinyint(1)]
 * @property int $port [int(11)]
 * @property float $time [float]
 * @property bool $type [tinyint(11)]
 * @property int $updated_at [int(11)]
 * @property int $created_at [int(11)]
 * @property int $checked_at [int(11)]  proverrenni server ili net. To est on voobsche bil rabochii srazu posle
 * @property bool $server_up [tinyint(1)]  server rabochii ili net
 * @property int $createdAt [int(11)]
 * @property int $updatedAt [int(11)]
 * @property string $error oshibka kotorya generitsya pri zaprose na dostupnost proxy
 * @property string $speed [varchar(10)]
 * @property int $good_bad [int(11)]
 * @property int $good_good [int(11)]
 * @property int $bad_bad [int(11)]
 * @property int $bad_good [int(11)]
 * @property string $source [varchar(100)]
 */
class Proxy extends ActiveRecord
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['ip', 'country', 'country_code'], 'string'],
            ['google', 'boolean'],
            ['time', 'double'],
            [['createdAt', 'updatedAt'], 'integer']
        ];
    }

    /**
     * @param string $ip
     * @param int $port
     * @param float $time
     * @param boolean $google
     * @param string $country_code
     * @param string $country
     * @param int $createdAt
     * @param int $updatedAt
     * @param string $source
     * @return Proxy
     */
    public static function create($ip, $port, $time = null, $google = false, $country_code = null, $country = null, $createdAt = null, $updatedAt = null, $source)
    {
        $proxy = new self();
        $proxy->ip = $ip;
        $proxy->port = $port;
        $proxy->time = $time;
        $proxy->google = $google;
        $proxy->country_code = strtoupper($country_code);
        $proxy->country = $country;
        $proxy->server_up = false;
        $proxy->checked_at = null;
        $proxy->updatedAt = strtotime($updatedAt);
        $proxy->createdAt = strtotime($createdAt);
        $proxy->source = $source;

        return $proxy;
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%proxies}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
        ];
    }
}
