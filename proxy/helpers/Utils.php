<?php

namespace console\helpers;

use console\models\Proxy;
use console\repositories\mailRepositories\sendMail;
use Yii;

/**
 * Class Utils
 * @package console\helpers
 */
class Utils {
    const LOG_DIR = 'runtime/logs/';

    public static function log(string $str, string $file_name, sendMail $mailer = null) {
        if (!file_exists(self::LOG_DIR))
            mkdir(self::LOG_DIR);

        $log = '[' . date('Y-m-d H:i:s') . '] ' . $str . PHP_EOL;

        if ($mailer) $mailer->sendEmailGeneralError('Error!', $log);

        $path_file = self::LOG_DIR . $file_name . '.log';

        $fo = fopen($path_file, 'a');
        fwrite($fo, $log);
        fclose($fo);
    }

    public static function checkParseOrNot(): bool {
        $settings = Yii::$app->db->createCommand('SELECT * FROM settings')->queryOne();
        return $settings['image_parse_on_off'];
    }

    public static function rotation_proxy(Proxy &$proxy, $bad = false): void {
        if ($bad) {
            if ($proxy->server_up) {
                $proxy->good_bad++;
                $proxy->server_up = 0;
            }
            else
                $proxy->bad_bad++;
        }
        else {
            if ($proxy->server_up)
                $proxy->good_good++;
            else {
                $proxy->bad_good++;
                $proxy->server_up = 1;
            }
        }
        $proxy->checked_at = time();
    }

    public static function curlRequest(string $url, array $headers = [], ?string $proxy = null, bool $return_binary = false) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($url));
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if ($headers)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($proxy)
            curl_setopt($ch, CURLOPT_PROXY, 'http://' . $proxy);

        if ($return_binary)
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

        $page = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);

        if ($httpCode != 200)
            return false;

        return $page;
    }
}