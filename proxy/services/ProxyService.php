<?php

namespace console\services;

use console\repositories\proxyRepositories\ProxyRepository;
use console\repositories\proxyRepositories\ProxyReadRepository;
use console\helpers\Utils;
use console\models\Proxy;
use IP2Location\Database;
use Symfony\Component\DomCrawler\Crawler;
use Yii;

/**
 * Class AddProductService
 * @package console\services\addProduct
 * @property  ProxyRepository $proxyRepository
 * @property  ProxyReadRepository $proxyReadRepository
 */
class ProxyService
{
    private $proxyRepository;
    private $proxyReadRepository;

    /**
     * ProxyService constructor.
     * @param ProxyRepository $proxyRepository
     * @param ProxyReadRepository $proxyReadRepository
     */
    public function __construct(ProxyRepository $proxyRepository, ProxyReadRepository $proxyReadRepository)
    {
        $this->proxyRepository = $proxyRepository;
        $this->proxyReadRepository = $proxyReadRepository;
    }

    public function newProxies($response)
    {
        if (isset($response['data']) && count($response['data']) > 0) {
            foreach ($response['data'] as $proxy) {
                $proxy_db = Proxy::create(
                    $proxy['ip'],
                    $proxy['port'],
                    $proxy['time'],
                    $proxy['google'],
                    $proxy['country_code'],
                    $proxy['country'],
                    $proxy['createdAt'],
                    $proxy['updatedAt'],
                    'proxy11'
                );
                $this->proxyRepository->save($proxy_db);
            }
        }
    }

    /**
     * @param  $url
     * @throws \Exception
     */
    public function newProxiesProxyList($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);

        $response = curl_exec($ch);
        // print_r($response);
        $data = explode(PHP_EOL, $response);
        array_pop($data);
        foreach ($data as $line) {

            echo $line . PHP_EOL;
            $proxy_explode = explode(':', $line);

            $ip_proxy = $proxy_explode[0];
            $port_proxy = $proxy_explode[1];

            if(filter_var($ip_proxy, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)){
                $line_array[] = $line;
                echo $line . PHP_EOL;

                $proxy = Proxy::create(
                    $ip_proxy,
                    $port_proxy,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    'proxy-list.download'
                );

                $proxy_saved = $this->proxyRepository->save($proxy);

                //__ ustanavlivaem stranu etomu proxy
                $proxy_for_update_ISO = $this->proxyReadRepository->getProxy($proxy_saved->id);
                // $ISO = IpHelper::IpToInt($proxy_for_update_ISO->ip);

                $db = new Database(Yii::getAlias('@console') . '/assets/IP2LOCATION-LITE-DB1.BIN', Database::FILE_IO);

                $records = $db->lookup($proxy_for_update_ISO->ip, Database::ALL);
                $proxy_for_update_ISO->country_code = $records['countryCode'];
                $proxy_for_update_ISO->country = $records['countryName'];

                $this->proxyRepository->save($proxy_for_update_ISO);

            }

        }
    }

    public function newFreeProxyList(string $url): void {
        if ($page = Utils::curlRequest($url)) {
            $document = new Crawler($page);
            foreach ($document->filter('#list tbody > tr') as $proxy) {
                $proxy = new Crawler($proxy);
                $tds = $proxy->filter('td');
                if (trim($tds->eq(4)->text()) == "elite proxy" or trim($tds->eq(6)->text()) == "yes") {
                    $time = time();

                    $proxy_db = Proxy::create(
                        $tds->eq(0)->text(),
                        (int) $tds->eq(1)->text(),
                        null,
                        ($tds->eq(5)->text() == 'yes'),
                        $tds->eq(2)->text(),
                        $tds->eq(3)->text(),
                        $time,
                        $time,
                        'freeProxy'
                    );
                    $this->proxyRepository->save($proxy_db);
                }
            }
        }
        else
            throw new \Exception('Ne udalos podkluchitsa k saitu');
    }


    /**
     * @throws \Exception
     */
    public function checkAvailability()
    {
        //__ Voboraem te kotorie tolko zakachani v BD bili
        $all_not_checked = Proxy::find()->where(['checked_at' => null]);

        /** @var Proxy $proxy_not_checked */
        foreach ($all_not_checked->each(100) as $proxy_not_checked) {
            try {
                $proxy_not_checked->checked_at = time();
                if ($speed = $this->curlCheckAvailability($proxy_not_checked->ip, $proxy_not_checked->port)) {
                    $proxy_not_checked->speed = $speed;
                    $proxy_not_checked = $this->returnStatus($proxy_not_checked, true);
                    $proxy_not_checked->error = null;
                    $proxy_not_checked->save(false);

                    echo 'sait ne dostupen - ' . $proxy_not_checked->ip . PHP_EOL;
                };
            } catch (\Exception $e) {
                echo $e->getMessage();
                $this->returnStatus($proxy_not_checked, false);
                $proxy_not_checked->error = $e->getMessage();
                $proxy_not_checked->save(false);
            }
        }
    }

    /**
     * @comment Delaem probavki dlya togo chto bi ponyat kogda kakoi server lejit ili rabotaet
     *
     * @param Proxy $proxy
     * @param bool $status
     * @return Proxy
     */
    private function returnStatus(Proxy $proxy, $status)
    {
        switch ($status) {
            case true:

                if (!$proxy->error) {
                    $proxy->good_good += 1;
                } else {
                    $proxy->bad_good += 1;
                }
                break;

            case false:
                echo "false case" . PHP_EOL;
                echo "oshibka est ili net: " . $proxy->error . 'klklk' . PHP_EOL;

                if (!$proxy->error) {

                    $proxy->good_bad += 1;
                } else {
                    $proxy->bad_bad += 1;
                }
                break;
        }
        return $proxy;
    }

    /**
     * @param $ip
     * @param $port
     * @return int
     * @throws \Exception
     */
    private function curlCheckAvailability($ip, $port)
    {
        $passByIPPort = $ip . ":" . $port;

// You can use any web site here to show you real ip such as http://whatismyipaddress.com/
        $url = "https://pizcool.com";

// Get current time to check proxy speed
        $loadingtime = time();

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_PROXY, $passByIPPort);

//Execute the request
        $curlResponse = curl_exec($ch);

        if ($curlResponse === false) {
            throw new \Exception(curl_error($ch));
            //  echo "Proxy is not working: ", curl_error($ch);
        } else {
            return (time() - $loadingtime);
        }
    }

    public function checkProxy() {
        Proxy::deleteAll('bad_bad > 3');
    }
}
