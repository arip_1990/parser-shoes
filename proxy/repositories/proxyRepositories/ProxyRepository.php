<?php

namespace console\repositories\proxyRepositories;

use console\models\Proxy;
use DomainException;

class ProxyRepository
{
    /**
     * @param Proxy $proxy
     * @return Proxy
     */
    public function save(Proxy $proxy)
    {
        if (!$proxy->save(false)) {
            throw new DomainException("Not saved proxy!");
        }
        return $proxy;
    }
}
