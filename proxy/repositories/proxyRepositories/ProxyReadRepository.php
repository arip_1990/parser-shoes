<?php

namespace console\repositories\proxyRepositories;

use console\models\Proxy;

class ProxyReadRepository
{
    /**
     * @param int $id
     * @return Proxy
     */
    public function getProxy($id)
    {
        return Proxy::findOne($id);
    }
}
