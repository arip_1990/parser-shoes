<?php
$params = array_merge(
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(__DIR__) . '/vendor',
    'bootstrap' => [
        'queue',
        'log',
        'console\bootstrap\SetUp'
    ],
    'controllerNamespace' => 'console\controllers',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=db;dbname=parser',
            'username' => 'root',
            'password' => 'secret',
            'charset' => 'utf8',
        ],
        // 'db1' => [
        //     'class' => 'yii\db\Connection',
        //     'dsn' => 'mysql:host=db;dbname=tripadvizor',
        //     'username' => 'root',
        //     'password' => '12345',
        //     'charset' => 'utf8',
        // ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@console/mail',
        ],
        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // DB connection component or its config
            'tableName' => '{{%yii_queue}}', // Table name
            // 'channel' => 'default', // Queue channel key
            'mutex' => \yii\mutex\MysqlMutex::class, // Mutex that used to sync queries
            'as log' => \yii\queue\LogBehavior::class,
            // 'deleteReleased' => YII_ENV_PROD
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ]
    ],
    'params' => $params,
];
