<?php

namespace console\repositories\shopRepositories;

use console\models\Shops;

class ShopReadRepository {
    public function getIdByCategory(string $category): int {
        if (!$shop = Shops::find()->where(['name' => $category])->limit(1)->one())
            throw new \Exception('Net Takogo magazina po kategorii');

        return $shop['id'];
    }
}