<?php

namespace console\extended;

use console\helpers\Utils;
use console\shops\GeneralShopClass;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;

class MultiParser {
    private $client,
            $proxies = [],
            $proxy = null,
            $urls = [],
            $data = [],
            $bad_proxies = [],
            $errors = [];

    public function __construct() {
        $this->proxies = Utils::getProxies();
    }

    public function scrape(array $urls, callable $callback, int $concurrency = 10, float $timeout = 10.0): void {
        $this->urls = $urls;
        $requests = function ($urls) {
            foreach ($urls as $url)
                yield new Request('GET', trim($url));
        };

        $this->setHeaders($timeout);

        $pool = new Pool($this->client, $requests($this->urls), [
            'concurrency' => $concurrency,
            'fulfilled' => function ($response, $index) use ($callback) {
                if ($data = call_user_func($callback, (string) $response->getBody(), $this->urls[$index]))
                    $this->data[] = $data;
                
                Utils::rotation_proxy($this->proxy);
                if(GeneralShopClass::LAUNCH_CONSOLE)
                    echo 'url: ' . $this->urls[$index] . "\tproxy: " . $this->proxy->ip . ':' . $this->proxy->port . ' -> ok' . PHP_EOL;
            },
            'rejected' => function ($reason, $index) {
                Utils::rotation_proxy($this->proxy, true);
                $this->bad_proxies[] = $this->proxy;

                if(GeneralShopClass::LAUNCH_CONSOLE)
                    echo 'url: ' . $this->urls[$index] . "\tproxy: " . $this->proxy->ip . ':' . $this->proxy->port . ' -> error' . PHP_EOL;

                $this->proxy = null;
                $this->errors[] = ['url' => $this->urls[$index], 'message' => $reason];
            },
        ]);

        $promise = $pool->promise();
        $promise->wait();
    }

    private function setHeaders(float $timeout): void {
        $stack = new HandlerStack();
        $stack->setHandler(new CurlHandler());
        $stack->push($this->getProxy(function() {
            if (!$this->proxy) {
                if (!count($this->proxies)) throw new \Exception('Прокси закончились');
                $this->proxy = array_shift($this->proxies);
            }

            return 'http://' . $this->proxy->ip . ':' . $this->proxy->port;
        }));

        $this->client = new Client(['handler' => $stack, 'verify' => false, 'timeout' => $timeout]);
    }

    private function getProxy(callable $callback): string {
        return function (callable $handler) use ($callback) {
            return function (RequestInterface $request, $options) use ($handler, $callback) {
                return $handler($request, ['proxy' => $callback()]);
            };
        };
    }

    public function getData(): array {
        return $this->data;
    }

    public function getBadProxies(): array {
        if ($this->proxy) $this->bad_proxies[] = $this->proxy;
        
        if (count($this->proxies))
            foreach ($this->proxies as $proxy) $this->bad_proxies[] = $proxy;
        
        return $this->bad_proxies;
    }

    public function getErrors(): array {
        return $this->errors;
    }
}