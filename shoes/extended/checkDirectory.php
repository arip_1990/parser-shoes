<?php

namespace console\extended;

use console\models\Exceptions;
use console\models\Query;
use console\models\Shops;
use console\helpers\ExceptionsLog;
use console\helpers\Utils;
use console\interfaces\ShopInterface;
use console\repositories\mailRepositories\sendMail;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

class checkDirectory {
    private $mailer;
    private $name_shop;        //___ 'nike'
    private $path_to_shop;     //__ /var/www/learning/static/nike
    private $work_directory;  //__ 15897979
    private $new_updated;  //__ Sudya po tomu est li katalog dlya magazin v destination directory vistavlyaem znachenie
    private $count_images_insert = 0;  //__ Kolichestvo perenesennix kartinok
    private $time_execute_move_images;  //__ Vremya vipolneniya kartinok v papku nike
    private $time_execute_delete_images;  //__ Vremya na udaleniye kartinok
    private $total_images = 0;  //__ Total Images
    private $count_insert = 0;  //__ Total INSERT kotorie vipolnenie bili

    public function __construct(sendMail $mailer = null)
    {
        $this->mailer = $mailer;

        if (!file_exists(ShopInterface::PUBLIC_IMAGE_DIR))
            mkdir(ShopInterface::PUBLIC_IMAGE_DIR);

        if (!file_exists(ShopInterface::DIR_STATIC))
            mkdir(ShopInterface::DIR_STATIC);
    }

    private function checkExceptionShop(string $shop_name): bool
    {
        //__ Iskluchaem katalogi v kotorie ne nujno zaxodit
        // tak kak tam chto to ne pravilno i ix nujn budet udalit posle manualnogo analiza
        $shop_directories_for_exclude = ArrayHelper::getColumn(Exceptions::find()->all(), 'name_shop');
        if (!in_array($shop_name, $shop_directories_for_exclude))
            return true;
        return false;
    }

    public function check(array $list_shop_directory): void
    {
        //__ Ubiraem iz massiva tex kto sidit v exceptions tablice
        if (Exceptions::find()->where(['name_shop' => $this->name_shop])->one())
            throw new \Exception('Oshibka kakaya-to');
        $list_shop_directory = array_filter($list_shop_directory, [$this, 'checkExceptionShop']);

        foreach ($list_shop_directory as $value)
            $this->checkStructureTimeStamp($this->path_to_shop . DIRECTORY_SEPARATOR . $value);
    }

    function sort_in_array($a, $b): int
    {
        if ($a == $b) return 0;
        return ($a > $b) ? -1 : 1;
    }

    public function init(string $name_shop, string $images_dir): void
    {
        $this->name_shop = $name_shop;
        $controller_name = explode('_', $this->name_shop)[0];

        try {
            $this->path_to_shop = ShopInterface::DIR_STATIC . DIRECTORY_SEPARATOR . $this->name_shop;

            //__ Naxodim podxodyaschee imya papki iz kotoroi budem brat potom faili.
            $list_shop_directory = Utils::scanDir($this->path_to_shop);

            if (!count($list_shop_directory))
                throw new \Exception('Net papok v magazine ' . $this->name_shop);

            if (count($list_shop_directory) > ShopInterface::MAX_NUMBER_PER_DIRECTORY)
                throw new \Exception('popitka sozdaniya papki ' . $this->path_to_shop . ' bolshe chem nujno');

            $this->check($list_shop_directory);

            $exist = Shops::find()->where(['name' => $this->name_shop])->scalar();
            if (!$exist)
                throw new \Exception('Net takogo magazina v BD !'); //___ Esli net takogo magazina zaregenovago v BD

            $query = new Query();
            $query->name_shop = $this->name_shop; //'All';
            $query->status = ShopInterface::PROCESSING;
            $query->explication = 'Nachal perenos images';
            $query->save();

            $this->checkDirectoryShop($list_shop_directory, $images_dir);

            echo 'Ok' . PHP_EOL;
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage(), $controller_name);
            throw new \Exception($e->getMessage());
        }
        finally {
            Query::deleteAll(['status' => Query::PROCESSING]);
        }
    }

    private function checkDirectoryShop(array $list_dir_data, string $images_dir): void
    {
        if (count($list_dir_data)) {
            try {
                //__Proveryaem chto v papke tolko direktori i nichgo drugoe
                foreach ($list_dir_data as $dir)
                    if (!is_dir($this->path_to_shop . DIRECTORY_SEPARATOR . $dir))
                        throw new \Exception('Est kakoi to katalog kotorii ne katalog: ' . $this->path_to_shop . DIRECTORY_SEPARATOR . $dir);

                //__ Videlyaem imaya papki poslednei papki
                $this->work_directory = array_pop($list_dir_data);

                foreach ($list_dir_data as $dir)
                    Utils::removeDirectory($this->path_to_shop . DIRECTORY_SEPARATOR . $dir);

                $start_move = microtime(true);

                $path_images = $this->path_to_shop . DIRECTORY_SEPARATOR . $this->work_directory . DIRECTORY_SEPARATOR . $images_dir;
                echo 'path_images: ' . $path_images . PHP_EOL;
//                $this->copy_folder($path_images, ShopInterface::PUBLIC_IMAGE_DIR);
                $this->moveImages($path_images, ShopInterface::PUBLIC_IMAGE_DIR);
                Utils::removeDirectory($path_images);

                $this->time_execute_move_images = (microtime(true) - $start_move);
            }
            catch (\Exception $e) {
                throw new \Exception($e->getMessage() . ' --- ' . $e->getLine());
            }
        }
        else  //___ esli net failov-direktorii v papke to udalyaem pustoi katalog
            rmdir($this->path_to_shop);
    }

    private function moveImages(string $image_directory_from, string $image_directory_to): void
    {
        if (!is_dir($image_directory_to))
            throw new \Exception($image_directory_to . 'is not a directory!');

        if (!is_writable($image_directory_to))
            throw new \Exception($image_directory_to . 'is not writable!');

        if ($handle = opendir($image_directory_from)) {
            while (false !== ($file = readdir($handle))) {
                if (is_file($image_directory_from . '/' . $file)) {
                    rename($image_directory_from . '/' . $file, $image_directory_to . '/' . $file);
                    $this->count_images_insert++; //__ Privavlyaem na 1 kolichestov failo perekopirovanix
                }
            }
            closedir($handle);
        }
        else {
            closedir($handle);
            throw new \Exception($image_directory_from . 'could not be opened.');
        }
    }

    private function copy_folder(string $d1, string $d2, bool $upd = true, bool $force = true): void
    {
        if (is_dir($d1)) {
            $d2 = $this->mkdir_safe($d2, $force);
            if (!$d2)
                throw new \Exception('Oshibka kopirovaniya');

            $d = dir($d1);
            while (false !== ($entry = $d->read())) {
                if ($entry != '.' && $entry != '..') {
                    echo 'copy: ' . $d1 . DIRECTORY_SEPARATOR . $entry . PHP_EOL;
                    $this->copy_folder($d1 . DIRECTORY_SEPARATOR . $entry, $d2 . DIRECTORY_SEPARATOR . $entry, $upd, $force);
                }
            }

            $d->close();
        }
        else {
            $ok = $this->copy_safe($d1, $d2, $upd);
            if ($ok)
                $this->count_images_insert++;

            $this->total_images++;
        }
    }

    private function mkdir_safe(string $dir, bool $force)
    {
        if (file_exists($dir)) {
            if (is_dir($dir)) return $dir;
            else if (!$force) return false;
            unlink($dir);
        }

        if (mkdir($dir, 0775, true))
            return $dir;
        else
            return false;
    }

    private function copy_safe(string $f1, string $f2, bool $upd): bool
    {
        $time1 = filemtime($f1);

        if (file_exists($f2)) {
            $time2 = filemtime($f2);
            if ($time2 >= $time1 && $upd) return false;
        }

        $ok = copy($f1, $f2);
        if ($ok) {
            touch($f2, $time1);
            chmod($f2, 0775);
        }

        return $ok;
    }

    private function checkStructureTimeStamp(string $path_directory): void
    {
        echo 'path_dir: ' . $path_directory . PHP_EOL;
        //__ proveryaem est li images papka i eto papka
        if (!file_exists($path_directory) || !is_dir($path_directory))
            throw new \Exception('Direktoriya dlya kartinok  ' . $path_directory . ' ne suschestvuet');
    }

    private function sqlExecute(string $sql_file): void
    {
        $db = Yii::$app->db;
        //__ Chitaem fail sql i esli budut problemi so vstavkoi budet otkat
        $transaction = $db->beginTransaction();

        foreach ($this->generator($sql_file) as $line) {
            try {
                //  echo "V generatore";
                //__ Podschitivaem voobsche vsego INSERT posle kajdogo
                $findme = 'INSERT';
                $pos = stripos($line, $findme);  //__ Bez ucheta registra

                if ($pos === false)  //__ Obyazatelno sostavit tut false !!!
                    $this->count_insert++;

                if (!empty($line))
                    $db->createCommand(str_replace(array("\r", "\n"), '', $line))->execute();
            }
            catch (Exception $e) {
                $log_error = new ExceptionsLog();
                $log_error->insert(Exceptions::ERROR,
                    $this->name_shop,
                    'Oshibka v sql zaprose. ' . $this->name_shop . ' .Oshibka: ' . $e->getMessage(),
                    222
                );

                throw new \Exception("Wrong Sql request !!! " . $e->getMessage() . ' . sql file: ' . $sql_file);
            }
        }

        $transaction->commit();
    }

    private function generator(string $file_path): \Generator
    {
        $f = fopen($file_path, 'r');
        try {
            while ($line = fgets($f)) yield $line;
        }
        finally {
            fclose($f);
        }
    }
}
