<?php

namespace console\interfaces;

interface ShopGeneralInterface {
    public function parseForList(array $proxies): void;

    public function parseProduct(array $proxies): void;
}


