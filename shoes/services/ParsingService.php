<?php

namespace console\services;

use console\repositories\shopRepositories\ShopReadRepository;
use console\extended\checkDirectory;
use console\extended\MultiParser;
use console\interfaces\ShopGeneralInterface;
use yii\caching\Cache;

class ParsingService
{
    private $cache, $shopReadRepositories;

    public function __construct(Cache $cache, checkDirectory $checkDirectory, ShopReadRepository $shopReadRepository)
    {
        //$this->parserModel = new ParserModel($cache, $checkDirectory);
        $this->cache = $cache;
        $this->shopReadRepositories = $shopReadRepository;
    }

    public function scanForList(ShopGeneralInterface $shopGeneral, array $proxies): void
    {
        $shopGeneral->parseForList($proxies);
    }

    public function parseProduct(ShopGeneralInterface $shopGeneral, array $proxies): void
    {
        $shopGeneral->parseProduct($proxies);
    }
}
