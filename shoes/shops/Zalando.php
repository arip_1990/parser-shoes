<?php
namespace console\shops;

use console\helpers\StringHelper;
use console\models\Process;
use console\models\Query;
use console\helpers\Utils;
use console\interfaces\ShopGeneralInterface;
use Symfony\Component\DomCrawler\Crawler;

class Zalando extends  GeneralShopClass implements ShopGeneralInterface
{
    const NAME_MEN = 'zalando_men';

    public function parseForList(array $proxies): void
    {
        Utils::checkDirectoryForListProductCreated();
        Utils::checkScanList($this->category);
        $this->initProccess();

        try {
            $pages = 2;
            $i = 1;
            $proxy = null;
            while ($i <= $pages) {
                if ($proxy === null) {
                    if (count($proxies))
                        $proxy = array_shift($proxies);
                    else
                        throw new \Exception('Прокси закончились');
                }

                if(GeneralShopClass::LAUNCH_CONSOLE)
                    echo $this->url . $i . ' ---> ' . $proxy->ip . ':' . $proxy->port;

                try {
                    $page = Utils::curlRequest($this->url . $i, $proxy->ip . ':' . $proxy->port);
                }
                catch (\Exception $e) {
                    echo ' -> Error' . PHP_EOL;
                    Utils::rotation_proxy($proxy, true);
                    $this->proxies[] = $proxy;
                    $proxy = null;
                    continue;
                }

                Utils::rotation_proxy($proxy);
                Process::updateActivity($this->shop_id, Process::LIST_MAKE, $page);
                $document = new Crawler($page);
                $document = $document->filter('#z-nvg-cognac-root');

                if ($document->count()) {
                    echo ' -> Ok' . PHP_EOL;
                    foreach ($document->filter('z-grid-item.cat_card-1o_9G a.cat_imageLink-OPGGa') as $link)
                        $this->data[] = 'https://www.zalando.fr' . trim($link->getAttribute('href'));

                    if ($i === 1 || $pages === 2) {
                        $pag = $document->filter('.cat_label-2W3Y8');
                        if (count($pag)) {
                            preg_match_all('/\d+/', $pag->eq(0)->text(), $matches);
                            $pages = (int) $matches[0][1];
                        }
                    }
                }

                $i++;
            }

            if ($proxy)
                $this->proxies[] = $proxy;
        }
        catch (\Exception $e) {
            // $this->mailer->sendEmailGeneralError($category . ' ' . $this->_name_directory, $e->getMessage());
            throw new \Exception($this->category . '. ' . $this->_name_directory . '. ' . $e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine());
        }
        finally {
            $this->db->createCommand('DELETE FROM ' . Query::tableName() . ' WHERE name_shop = "' . $this->category . '"')->execute();
        }
    }

    public function extractFromHtml(string $html, string $url): void
    {
        $timeStamp = time();

        $document = new Crawler($html);
        $cdata = $document->filter('#z-vegas-pdp-props');
        $name = $document->filter("div[data-re] h1[tag='h1']");
        unset($document);

        if (!count($name) || !count($cdata))
            throw new \Exception('Нет данных на сайте: ' . $url);

        Process::updateActivity($this->shop_id, Process::PARSING_PRODUCT, $html);

        $cdata = json_decode(str_replace(['<![CDATA[', ']]>'], '', htmlspecialchars_decode($cdata->eq(0)->text())));
        $product = $cdata->model->articleInfo;
        unset($cdata);

        $sizes = [];
        for ($j = 0; $j < count($product->units); $j++)
            $sizes[] = trim($product->units[$j]->size->local);

        if (!count($sizes))
            throw new \Exception('Нет размеров на сайте: ' . $url);

        $price = (float)str_replace(',', '.', $product->displayPrice->price->value);
        $reference = trim($product->id);

        $data = [];
        $row_exist = $this->db->createCommand('SELECT id, product_id FROM shoes WHERE reference = "' . $reference . '";')->queryOne();
        if ($row_exist) { // __ Esli u nas est yje takoi produkt v BD
            $data['update'] = true;

            $data['shoes']['id'] = $row_exist['id'];
            $data['shoes']['price'] = $price;
            $data['shoes']['timeStamp'] = $timeStamp;

            $data['size']['timeStamp'] = $timeStamp;
            $data['size']['sizes'] = [];
            foreach ($sizes as $size)
                $data['size']['sizes'][] = $size;
        }
        else {  //__ Vooobsche net
            $data['update'] = false;

            $data['product']['shop_id'] = (int) $this->shop_id;
            $data['product']['type_id'] = 1;
            $data['product']['timeStamp'] = $timeStamp;

            $data['shoes']['brand'] = StringHelper::slashEscape(trim($product->brand->name));
            $data['shoes']['name'] = StringHelper::slashEscape(trim($name->text()));
            $data['shoes']['color'] = StringHelper::slashEscape(trim($product->color));
            $data['shoes']['currency'] = $this->getCurrencyInt($product->displayPrice->price->currency);
            $data['shoes']['url'] = $url;
            $data['shoes']['sex'] = $this->category === self::NAME_MEN ? 0 : 1;
            $data['shoes']['price'] = $price;
            $data['shoes']['reference'] = $reference;
            $data['shoes']['timeStamp'] = $timeStamp;

            $images = [];
            for ($j = 0; $j < count($product->media->images); $j++)
                $images[] = trim($product->media->images[$j]->sources->zoom);

            $attributes = $product->attributes;
            unset($product);

            for ($j = 0; $j < count($attributes[0]->data); $j++) {
                if (array_key_exists('name', $attributes[0]->data[$j]) && isset($attributes[0]->data[$j]->name)) {
                    if (false !== mb_stripos(trim($attributes[0]->data[$j]->name), 'Dessus', 0, 'UTF-8'))
                        $data['shoes']['dessus'] = StringHelper::slashEscape(trim($attributes[0]->data[$j]->values));
                    elseif (false !== mb_stripos(trim($attributes[0]->data[$j]->name), 'Doublure', 0, 'UTF-8'))
                        $data['shoes']['doublure'] = StringHelper::slashEscape(trim($attributes[0]->data[$j]->values));
                    elseif (false !== mb_stripos(trim($attributes[0]->data[$j]->name), 'propre', 0, 'UTF-8'))
                        $data['shoes']['semelle_de_proprete'] = StringHelper::slashEscape(trim($attributes[0]->data[$j]->values));
                    elseif (false !== mb_stripos(trim($attributes[0]->data[$j]->name), "d'usure", 0, 'UTF-8'))
                        $data['shoes']['semelle_usure'] = StringHelper::slashEscape(trim($attributes[0]->data[$j]->values));
                }
            }
            for ($j = 0; $j < count($attributes[1]->data); $j++) {
                if (array_key_exists('name', $attributes[1]->data[$j]) && isset($attributes[1]->data[$j]->name)) {
                    if (false !== mb_stripos(trim($attributes[1]->data[$j]->name), 'Fermeture', 0, 'UTF-8'))
                        $data['shoes']['fermeture'] = StringHelper::slashEscape(trim($attributes[1]->data[$j]->values));
                    elseif (false !== mb_stripos(trim($attributes[1]->data[$j]->name), 'Motif', 0, 'UTF-8'))
                        $data['shoes']['motif'] = StringHelper::slashEscape(trim($attributes[1]->data[$j]->values));
                }
            }
            unset($attributes);

            //___  Razmeri START
            $data['size']['timeStamp'] = $timeStamp;
            $data['size']['sizes'] = [];
            foreach ($sizes as $size)
                $data['size']['sizes'][] = $size;
            //___  Razmeri STOP

            //___ Kartinki START  ________
            if (count($images)) { //__ Esli voobsche est kartinki to skachivaem ix  + dobavlyaem v bazu
                $data['images']['timeStamp'] = $timeStamp;
                $data['images']['patch'] = [];
                foreach ($images as $image) {
                    $name_file = StringHelper::randomString(20, null, true);
                    if (Utils::saveImages($image, $this->_name_directory, $name_file, $data['shoes']['reference'])) //__ Esli kartinka est
                        $data['images']['patch'][] = $name_file . '.jpg';
                }
            } //___ Kartinki END ________
        }

        $this->data[] = json_encode($data);
    }
}
