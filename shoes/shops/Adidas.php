<?php

namespace console\shops;

use console\helpers\StringHelper;
use console\models\Process;
use console\models\Query;
use console\helpers\Utils;
use console\interfaces\ShopGeneralInterface;

class Adidas extends GeneralShopClass implements ShopGeneralInterface
{
    public function parseForList(array $proxies): void
    {
        Utils::checkDirectoryForListProductCreated();
        Utils::checkScanList($this->category);
        $this->initProccess();

        try {
            $proxy = null;
            for ($i = 0; $i < 20; $i++) {
                $url = $this->url . $i * 48;

                if ($proxy === null) {
                    if (count($proxies))
                        $proxy = array_shift($proxies);
                    else
                        throw new \Exception('Прокси закончились');
                }

                if(GeneralShopClass::LAUNCH_CONSOLE)
                    echo $url . ' ---> ' . $proxy->ip . ':' . $proxy->port;

                try {
                    $page = Utils::curlRequest($url . $i, $proxy->ip . ':' . $proxy->port);
                }
                catch (\Exception $e) {
                    echo ' -> Error' . PHP_EOL;
                    Utils::rotation_proxy($proxy, true);
                    $this->proxies[] = $proxy;
                    $proxy = null;
                    continue;
                }

                Utils::rotation_proxy($proxy);
                preg_match('/window\.DATA_STORE.+?\("(.+)"\)/', $page, $matches);

                if (count($matches)) {
                    echo ' -> Ok' . PHP_EOL;
                    Process::updateActivity($this->shop_id, Process::LIST_MAKE, $page);
                    $list_items = json_decode(stripslashes($matches[1]))->plp->itemList->items;
                    unset($matches);

                    foreach ($list_items as $list_item) {
                        $extrair_last_part = explode("/", $list_item->link);
                        $extrair_last_part = $extrair_last_part[count($extrair_last_part) - 1];
                        $all_colors = $list_item->colorVariations;  //__ Vse cveta v massive

                        $base_path_url = str_replace($extrair_last_part, '', $list_item->link);

                        foreach ($all_colors as $color)  //__ Delaem podstanovku s chvetami i obrazuem novie URL
                            $this->data[] = 'https://www.adidas.fr' . $base_path_url . $color . '.html';
                    }
                }
            }

            if ($proxy)
                $this->proxies[] = $proxy;
        }
        catch (\Exception $e) {
            //__ koment  $this->mailer->sendEmailGeneralError('Get url for parsing. ' . $this->category . ' ' . $this->_name_directory, $e->getMessage() . '. line:' . $e->getLine());
            throw new \Exception($this->category . '. ' . $this->_name_directory . '. ' . $e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine());
        }
        finally {
            $this->db->createCommand('DELETE FROM ' . Query::tableName() . ' WHERE name_shop = "' . $this->category . '" ')->execute();
        }
    }

    public function extractFromHtml(string $html, string $url): void
    {
        $timeStamp = time();
        Process::updateActivity($this->shop_id, Process::PARSING_PRODUCT, $html);

        $article = explode('/', $url);
        $article = explode('.', $article[count($article) - 1])[0];

        preg_match('/window\.DATA_STORE.+?\("(.+)"\)/', $html, $matches);

        if (!count($matches))
            throw new \Exception('Нет данных на сайте: ' . $url);

        $data = json_decode(stripslashes($matches[1]));
        $product = $data->productStore->products->{$article}->data;
        $currency = 'glass.iso-4217-currency';
        $currency = $data->app->translations->data->{$currency};

        if (isset($product->pricing_information->currentPrice))
            $price = $product->pricing_information->currentPrice;
        else
            $price = 0;

        $sizes = Utils::curlRequest("https://www.adidas.fr/api/products/tf/{$article}/availability");
        $sizes = json_decode($sizes)->variation_list;

        $data = [];
        $row_exist = $this->db->createCommand('SELECT article, id, product_id FROM shoes WHERE article = "' . $article . '";')->queryOne();
        if ($row_exist) { // __ Esli u nas est yje takoi produkt v BD to delaem UPDATE
            $data['update'] = true;

            $data['shoes']['id'] = $row_exist['id'];
            $data['shoes']['price'] = $price;
            $data['shoes']['timeStamp'] = $timeStamp;

            $data['size']['timeStamp'] = $timeStamp;
            $data['size']['sizes'] = [];
            foreach ($sizes as $item)
                $data['size']['sizes'][] = $item->size;
        }
        else {  //__ Esli u nas est net takoi produkt v BD to delaem INSERT
            $data['update'] = false;

            $data['product']['shop_id'] = (int) $this->shop_id;
            $data['product']['type_id'] = 1;
            $data['product']['timeStamp'] = $timeStamp;

            if (isset($product->product_description->text))
                $description = str_replace(["\r\n", "\r", "\n"], '', StringHelper::slashEscape(trim($product->product_description->text)));
            else
                $description = '';

            if (isset($product->product_description->usps[0]))
                $fermeture = StringHelper::slashEscape(trim($product->product_description->usps[0]));
            else
                $fermeture = '';

            if (isset($product->name))
                $name = StringHelper::slashEscape(trim($product->name));
            else
                $name = '';

            if (isset($product->attribute_list->color))
                $color = StringHelper::slashEscape(trim($product->attribute_list->color));
            else
                $color = '';

            if (isset($product->attribute_list->brand))
                $sub_name = 'Adidas ' . StringHelper::slashEscape(trim($product->attribute_list->brand));
            else
                $sub_name = 'Adidas';

            if (isset($product->attribute_list->brand))
                $brand = StringHelper::slashEscape(trim($product->attribute_list->brand));
            else
                $brand = 'Adidas';

            $data['shoes']['name'] = $name;
            $data['shoes']['sub_name'] = $sub_name;
            $data['shoes']['fermeture'] = $fermeture;
            $data['shoes']['currency'] = $this->getCurrencyInt($currency);
            $data['shoes']['url'] = $url;
            $data['shoes']['sex'] = $this->tableau_sex_ass[$this->category];
            $data['shoes']['brand'] = $brand;
            $data['shoes']['description'] = $description;
            $data['shoes']['color'] = $color;
            $data['shoes']['price'] = $price;
            $data['shoes']['article'] = $article;
            $data['shoes']['timeStamp'] = $timeStamp;

            //___  Razmeri START
            $data['size']['timeStamp'] = $timeStamp;
            $data['size']['sizes'] = [];
            foreach ($sizes as $item)
                $data['size']['sizes'][] = $item->size;
            //___  Razmeri STOP

            //___ Kartinki START  ________
            echo 'images list count: ' . count($product->view_list) . PHP_EOL;
            if (isset($product->view_list)) {
                $images = [];
                foreach ($product->view_list as $item)
                    if ($item->type == 'standard' and $item->image_url)
                        $images[] = str_replace('w_600', 'w_2000', $item->image_url);

                echo 'images count: ' . count($images) . PHP_EOL;
                if (count($images)) { //__ Esli voobsche est kartinki to skachivaem ix  + dobavlyaem v bazu
                    $data['images']['timeStamp'] = $timeStamp;
                    $data['images']['patch'] = [];
                    foreach ($images as $image) {
                        $name_file = StringHelper::randomString(20, null, true);
                        if (Utils::saveImages($image, $this->_name_directory, $name_file, $data['shoes']['article']))  //__ Esli kartinka est
                            $data['images']['patch'][] = $name_file . '.jpg';
                    }
                }
            } //___ Kartinki END ________
        }

        $this->data[] = json_encode($data);
    }
}
