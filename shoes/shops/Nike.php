<?php
namespace console\shops;

use console\helpers\StringHelper;
use console\models\Process;
use console\models\Query;
use console\helpers\Utils;
use console\interfaces\ShopGeneralInterface;

class Nike extends  GeneralShopClass implements ShopGeneralInterface
{
    public function parseForList(array $proxies): void
    {
        Utils::checkDirectoryForListProductCreated();
        Utils::checkScanList($this->category);
        $this->initProccess();

        try {
            $i = 1;
            $proxy = null;
            while (true) {
                if ($proxy === null) {
                    if (count($proxies))
                        $proxy = array_shift($proxies);
                    else
                        throw new \Exception('Прокси закончились');
                }

                if(GeneralShopClass::LAUNCH_CONSOLE)
                    echo $this->url . $i . ' ---> ' . $proxy->ip . ':' . $proxy->port;

                try {
                    $page = Utils::curlRequest($this->url . $i, $proxy->ip . ':' . $proxy->port);
                }
                catch (\Exception $e) {
                    echo ' -> Error' . PHP_EOL;
                    Utils::rotation_proxy($proxy, true);
                    $this->proxies[] = $proxy;
                    $proxy = null;
                    continue;
                }

                Utils::rotation_proxy($proxy);
                $data = json_decode($page);

                $i++;
                if (isset($data->sections)) {
                    echo ' -> Ok' . PHP_EOL;
                    Process::updateActivity($this->shop_id, Process::LIST_MAKE, $page);
                    $items = $data->sections[0]->items;
                    $nextPage = !!$data->nextPageDataService;

                    foreach ($items as $item)
                        if (isset($item->pdpUrl) && false !== mb_stripos(trim($item->pdpUrl), 'chaussure-', 0, 'UTF-8'))
                            $this->data[] = trim($item->pdpUrl);

                    if (!$nextPage)
                        break;
                }
            }

            if ($proxy)
                $this->proxies[] = $proxy;
        }
        catch (\Exception $e) {
            // $this->mailer->sendEmailGeneralError('Get url for parsing. ' . $category . ' ' . $this->_name_directory, $e->getMessage());
            throw new \Exception($this->category . '. ' . $this->_name_directory . '. ' . $e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine());
        }
        finally {
            $this->db->createCommand('DELETE FROM ' . Query::tableName() . ' WHERE name_shop = "' . $this->category . '"')->execute();
        }
    }

    public function extractFromHtml(string $html, string $url): void
    {
        $data = [];
        $timeStamp = time();
        Process::updateActivity($this->shop_id, Process::PARSING_PRODUCT, $html);

        preg_match_all('/(?<=&pbid=).*\d/', $url, $matches);

        if (count($matches[0])) { // Esli obuv s Nike Store
            preg_match_all('/(?<=js-pdpLocalPrice">)[\w\W]*?(?=<\/span>)/', $html, $matches_price);

            $price = trim($matches_price[0][0]);
            $price = substr($price, 0, -2);  //___ 160  Prosto chislo

            preg_match_all("#(?<=type=\"importModelTempData\"\ data-name=\"pdpData\">)[\w\W]*?\"showRecyclingMessage\":true}#", $html, $script);
            $peremennaya = json_decode($script[0][0]);
            $capacityMessage = $peremennaya->capacityMessage;

            $exist_pbid = $this->db->createCommand('SELECT * FROM shoes WHERE pbid ="' . $matches[0][0] . '"')->execute();
            if ($exist_pbid) { //___ Esli est takoi magazin u menya v BD
                $data['update'] = true;

                $data['shoes']['id'] = $exist_pbid['id'];
                $data['shoes']['delivery'] = substr($capacityMessage, 0, 200);
                $data['shoes']['price'] = (float) $price;
                $data['shoes']['timeStamp'] = $timeStamp;
            }
            else {
                $data['update'] = false;

                $data['product']['shop_id'] = (int) $this->shop_id;
                $data['product']['type_id'] = 1;
                $data['product']['timeStamp'] = $timeStamp;

                //___ Kartinki START  ________
                $images = [];
                foreach ($peremennaya->prebuilds as $prebuild) {
                    if ($prebuild->pbid == $matches[0][0])
                        foreach ($prebuild->views as $view)
                            $images[] = $view->url;
                }

                if (count($images)) { //__ Esli voobsche est kartinki to skachivaem ix  + dobavlyaem v bazu
                    $data['images']['timeStamp'] = $timeStamp;
                    $data['images']['patch'] = [];
                    foreach ($images as $image) {
                        $name_file = StringHelper::randomString(20, null, true);
                        if (Utils::saveImages($image, $this->_name_directory, $name_file, $timeStamp))  //__ Esli kartinka est
                            $data['images']['patch'][] = $name_file . '.jpg';
                    }
                }
                //___ Kartinki END  ________
            }
        }
        else {  //___ Esli obuv ne s Nike Store
            preg_match_all("#(?<=<script>window\.INITIAL_REDUX_STATE=)[\w\W]*?(?=;<\/script>)#", $html, $script);
            if (!isset($script[0][0]) or empty($script[0][0]))
                throw new \Exception('Нет данных на сайте: ' . $url);

            preg_match('/(?<=">Article\ :\ )[\w\W]*?(?=<\/li>)/', $html, $article);
            if (!count($article))
                throw new \Exception('Нет артикул на сайте: ' . $url);

            $article = $article[0];

            $product = json_decode($script[0][0])->Threads->products->{$article};
            unset($script);

            $brand = StringHelper::slashEscape($product->brand);
            $name = StringHelper::slashEscape($product->title);
            $sub_name = StringHelper::slashEscape($product->subTitle);
            $description = StringHelper::slashEscape($product->descriptionPreview);
            $color = $product->colorDescription;
            $price = (float) $product->currentPrice;
            $currency = $product->currency;
            $sex = ($product->genders[0] == "MEN") ? 0 : 1;
            $sizes = [];
            
            if (!count($product->availableSkus))
                throw new \Exception('Нет размеров на сайте: ' . $url);
            
            foreach ($product->skus as $skus) {
                foreach ($product->availableSkus as $available_skus) {
                    if ($skus->skuId == $available_skus->id)
                        $sizes[] = $skus->localizedSize;
                }
            }

            $row_exist = $this->db->createCommand('SELECT s.article, s.id, product_id FROM shoes s INNER JOIN product pr ON (pr.id = s.product_id) WHERE s.article = "' . $article . '";')->queryOne();

            if ($row_exist) { // __ Esli u nas est yje takoi produkt v BD
                $data['update'] = true;

                $data['shoes']['id'] = $row_exist['id'];
                $data['shoes']['price'] = $price;
                $data['shoes']['timeStamp'] = $timeStamp;

                $data['size']['timeStamp'] = $timeStamp;
                $data['size']['sizes'] = [];
                foreach ($sizes as $size)
                    $data['size']['sizes'][] = $size;
            }
            else {  //__ Vooobsche net
                $data['update'] = false;

                $data['product']['shop_id'] = (int) $this->shop_id;
                $data['product']['type_id'] = 1;
                $data['product']['timeStamp'] = $timeStamp;

                $data['shoes']['name'] = $name;
                $data['shoes']['sub_name'] = $sub_name;
                $data['shoes']['currency'] = $this->getCurrencyInt($currency);
                $data['shoes']['url'] = $url;
                $data['shoes']['sex'] = $sex;
                $data['shoes']['brand'] = $brand;
                $data['shoes']['description'] = $description;
                $data['shoes']['color'] = $color;
                $data['shoes']['price'] = $price;
                $data['shoes']['article'] = $article;
                $data['shoes']['timeStamp'] = $timeStamp;

                //___  Razmeri START
                $data['size']['timeStamp'] = $timeStamp;
                $data['size']['sizes'] = [];
                foreach ($sizes as $size)
                    $data['size']['sizes'][] = $size;
                //___  Razmeri STOP

                //___ Kartinki START  ________
                $images = [];
                foreach ($product->nodes[0]->nodes as $node)
                    if ($node->subType == 'image' && $node->properties->portraitURL)
                        $images[] = str_replace('t_default', 't_PDP_1280_v1', $node->properties->portraitURL);

                if (count($images)) { //__ Esli voobsche est kartinki to skachivaem ix  + dobavlyaem v bazu
                    $data['images']['timeStamp'] = $timeStamp;
                    $data['images']['patch'] = [];
                    foreach ($images as $image) {
                        $name_file = StringHelper::randomString(20, null, true);
                        if (Utils::saveImages($image, $this->_name_directory, $name_file, $data['shoes']['article'])) //__ Esli kartinka est
                            $data['images']['patch'][] = $name_file . '.jpg';
                    }
                }
                //___ Kartinki END ________
            }
        }

        $this->data[] = json_encode($data);
    }
}
