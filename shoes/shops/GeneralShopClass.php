<?php

namespace console\shops;

use console\helpers\Utils;
use console\interfaces\ShopInterface;
use console\models\Query;
use console\repositories\shopRepositories\ShopReadRepository;
use console\extended\checkDirectory;
use console\repositories\mailRepositories\sendMail;

/**
 * Class GeneralShopClass
 * @package console\shops
 * @property  int $last_product_id
 * @property  int $start_product_id
 * @property  ShopReadRepository $shopReadRepository
 * @property  int $got_last_product_id
 * @property  string $path_sql_path
 * @property  string $_name_directory
 * @property  checkDirectory $checkDirectory
 */
class GeneralShopClass
{
    const LAUNCH_CONSOLE = 1;
    const LAUNCH_WEB = 0;

    public $last_product_id;
    public $start_product_id;
    public $shopReadRepositories;
    public $got_last_product_id;
    public $path_sql_path;
    public $_name_directory;
    public $checkDirectory;
    public $tableau_sex_ass;
    public $console_web;
    public $url;
    public $category;
    public $proxies = [];
    public $data = [];

    protected $fo, $db, $shop_id;

    public function __construct(sendMail $mailer)
    {
        $this->shopReadRepositories = new  ShopReadRepository();
        $this->checkDirectory = new checkDirectory($mailer);
    }

    protected function initProccess(): void
    {
        $row_exists = Query::find()->where(['status' => ShopInterface::PROCESSING])->one();
        if ($row_exists)
            throw new \Exception('>>> Other query in processing. ' . $row_exists['explication']);

        $this->db = \Yii::$app->db;
        $this->shop_id = $this->shopReadRepositories->getIdByCategory($this->category);

        $query = new Query();
        $query->name_shop = $this->category;
        $query->status = ShopInterface::PROCESSING;
        $query->explication = 'Kachaem iz saita infu (kartinki + sql fail). Shop = ' . $this->category;
        $query->save();  // @todo aktivirovat
    }

    public function parseProduct(array $proxies): void
    {
        $path_to_list_file = ShopInterface::LIST_SHOP_PRODUCTS . DIRECTORY_SEPARATOR . $this->category;
        if (!file_exists($path_to_list_file) || !is_file($path_to_list_file))
            throw new \Exception('Net faila-spiska dlya ' . $this->category);

        if (!$this->_name_directory = Utils::prepareDirectory($this->category))
            throw new \Exception('Ne udalos sozdat timestamp papku');

        $lines = explode(PHP_EOL, file_get_contents(ShopInterface::LIST_SHOP_PRODUCTS . DIRECTORY_SEPARATOR . $this->category));

        $this->initProccess();

        try {
            $proxy = null;
            $url = array_pop($lines);
            while (count($lines)) {
                if (!$url) $url = array_pop($lines);
                if (!$proxy) {
                    if (count($proxies))
                        $proxy = array_shift($proxies);
                    else
                        throw new \Exception('Прокси закончились');
                }

                if(GeneralShopClass::LAUNCH_CONSOLE)
                    echo $url . ' ---> ' . $proxy->ip . ':' . $proxy->port;

                try {
                    $page = Utils::curlRequest($url, $proxy->ip . ':' . $proxy->port);
                    echo ' -> Ok' . PHP_EOL;
                }
                catch (\Exception $e) {
                    echo ' -> Error' . PHP_EOL;
                    Utils::rotation_proxy($proxy, true);
                    $this->proxies[] = $proxy;
                    $proxy = null;
                    continue;
                }

                Utils::rotation_proxy($proxy);
                try {
                    $this->extractFromHtml($page, $url);
                }
                catch (\Exception $e) {}
                $url = null;
            }

            if ($proxy)
                $this->proxies[] = $proxy;
        }
        catch (\Exception $e) {
            // $this->mailer->sendEmailGeneralError('Error of Generation sql file. ' . $category . '. ' . $this->_name_directory, $e->getLine() . ', ' . $e->getMessage() . PHP_EOL . $e->getTraceAsString());
            throw new \Exception($this->category . '. ' . $this->_name_directory . '. ' . $e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine());
        }
        finally {
            $this->db->createCommand('DELETE FROM ' . Query::tableName() . ' WHERE name_shop = "' . $this->category . '"')->execute();
        }
    }

    protected function getCurrencyInt(string $currency): ?int
    {
        switch ($currency) {
            case '€':
            case 'EUR':
                return 1;
            case '$':
            case 'US':
                return 2;
            case '£':
            case 'Â£':
                return 3;
            default:
                return null;
        }
    }

    protected function getMemory()
    {
        $unit = ['b','Kb','Mb','Gb','Tb'];
        $size = memory_get_usage(true);
        return @round($size / pow(1024,($i = floor(log($size,1024)))),2).' '.$unit[$i];
    }
}