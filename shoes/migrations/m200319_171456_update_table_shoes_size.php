<?php

use yii\db\Migration;

class m200319_171456_update_table_shoes_size extends Migration
{
    public function up()
    {
        $this->createTable('{{%shoes_size}}', [
            'id' => $this->primaryKey()->unsigned(),
            'size_us' => $this->string(10),
            'size_ru' => $this->string(10),
            'size_eu' => $this->string(10),
            'size_cm' => $this->string(10),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%shoes_size}}');
    }
}
