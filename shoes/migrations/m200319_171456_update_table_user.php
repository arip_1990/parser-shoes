<?php

use yii\db\Migration;

class m200319_171456_update_table_user extends Migration
{
    public function up()
    {
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'fb_id' => $this->string(100),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string(),
            'password_reset_token' => $this->string(),
            'email' => $this->string(),
            'email_confirm_token' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue('10'),
            'unknown' => $this->tinyInteger(1)->comment('0 - zaregennii po zaprosu tokena, 1 - zaregennii po forme'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->createIndex('email_confirm_token', '{{%user}}', 'email_confirm_token', true);
        $this->createIndex('password_reset_token', '{{%user}}', 'password_reset_token', true);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
