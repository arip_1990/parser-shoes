<?php

use yii\db\Migration;

class m200319_171456_update_table_auth_item_children extends Migration
{
    public function up()
    {
        $this->createTable('{{%auth_item_children}}', [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARYKEY', '{{%auth_item_children}}', ['parent', 'child']);
        $this->createIndex('child', '{{%auth_item_children}}', 'child');
    }

    public function down()
    {
        $this->dropTable('{{%auth_item_children}}');
    }
}
