<?php

use yii\db\Migration;

class m200319_171456_update_table_product extends Migration
{
    public function up()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey()->unsigned(),
            'type_id' => $this->integer(),
            'shop_id' => $this->integer(),
            'key' => $this->string(36),
            'test_product' => $this->tinyInteger(1)->notNull()->defaultValue('0')->comment('testovii produkt ili net. dlya testovix polzovatei'),
            'spell_checking_done' => $this->tinyInteger(1)->comment('proverka chto vse lingvisticheskie proverki proideni. Nujni dlya ispravleniya simvolov v stroke name ili brand il escho gde nujno budet'),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('product_key_index', '{{%product}}', 'key');
    }

    public function down()
    {
        $this->dropTable('{{%product}}');
    }
}
