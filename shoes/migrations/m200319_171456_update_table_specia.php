<?php

use yii\db\Migration;

class m200319_171456_update_table_specia extends Migration
{
    public function up()
    {
        $this->createTable('{{%specia}}', [
            'id' => $this->primaryKey(),
            'html_code' => $this->string(10),
            'symbol' => $this->string(10),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%specia}}');
    }
}
