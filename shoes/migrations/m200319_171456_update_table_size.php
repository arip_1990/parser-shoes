<?php

use yii\db\Migration;

class m200319_171456_update_table_size extends Migration
{
    public function up()
    {
        $this->createTable('{{%size}}', [
            'id' => $this->primaryKey()->unsigned(),
            'product_id' => $this->integer()->unsigned(),
            'size_us' => $this->string(11),
            'size' => $this->string(11),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('size_index', '{{%size}}', 'size');
        $this->createIndex('fk_product_id', '{{%size}}', 'product_id');
    }

    public function down()
    {
        $this->dropTable('{{%size}}');
    }
}
