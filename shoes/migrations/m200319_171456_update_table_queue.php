<?php

use yii\db\Migration;

class m200319_171456_update_table_queue extends Migration
{
    public function up()
    {
        $this->createTable('{{%queue}}', [
            'id' => $this->primaryKey(),
            'name_shop' => $this->string(100)->comment('nazvanie magazina'),
            'status' => $this->tinyInteger(1)->comment('0 - svobodna ochered, 1 - vipolnyaetsya'),
            'explication' => $this->string(250),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%queue}}');
    }
}
