<?php

use yii\db\Migration;

class m200319_171456_update_table_colors_on_image extends Migration
{
    public function up()
    {
        $this->createTable('{{%colors_on_image}}', [
            'id' => $this->primaryKey()->unsigned(),
            'image_id' => $this->integer()->unsigned(),
            'color_hex' => $this->string(10),
            'color_from_pallet' => $this->integer()->unsigned(),
            'wight' => $this->decimal(5, 4),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%colors_on_image}}');
    }
}
