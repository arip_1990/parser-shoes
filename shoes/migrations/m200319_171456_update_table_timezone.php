<?php

use yii\db\Migration;

class m200319_171456_update_table_timezone extends Migration
{
    public function up()
    {
        $this->createTable('{{%timezone}}', [
            'zone_id' => $this->integer(10)->notNull(),
            'abbreviation' => $this->string(6)->notNull(),
            'time_start' => $this->decimal(11, 0)->notNull(),
            'gmt_offset' => $this->integer()->notNull(),
            'dst' => $this->char()->notNull(),
        ]);

        $this->createIndex('idx_time_start', '{{%timezone}}', 'time_start');
        $this->createIndex('idx_zone_id', '{{%timezone}}', 'zone_id');
    }

    public function down()
    {
        $this->dropTable('{{%timezone}}');
    }
}
