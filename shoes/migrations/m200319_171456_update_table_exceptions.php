<?php

use yii\db\Migration;

class m200319_171456_update_table_exceptions extends Migration
{
    public function up()
    {
        $this->createTable('{{%exceptions}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name_shop' => $this->string(100)->notNull()->defaultValue('')->comment('nazvanie magazina'),
            'cause' => $this->text(),
            'status' => $this->tinyInteger(1)->comment('0 - svobodna ochered, 1 - vipolnyaetsya'),
            'code_error' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('shop_index', '{{%exceptions}}', 'name_shop');
    }

    public function down()
    {
        $this->dropTable('{{%exceptions}}');
    }
}
