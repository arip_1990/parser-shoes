<?php

use yii\db\Migration;

class m200319_171456_update_table_dedede extends Migration
{
    public function up()
    {
        $this->createTable('{{%dedede}}', [
            'id' => $this->primaryKey(),
            'de' => $this->string(45),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%dedede}}');
    }
}
