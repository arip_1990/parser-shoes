<?php

use yii\db\Migration;

class m200319_171456_update_table_search_parameter extends Migration
{
    public function up()
    {
        $this->createTable('{{%search_parameter}}', [
            'id' => $this->primaryKey()->unsigned(),
            'colorId' => $this->integer()->unsigned(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%search_parameter}}');
    }
}
