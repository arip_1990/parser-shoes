<?php

use yii\db\Migration;

class m200319_171456_update_table_proxies extends Migration
{
    public function up()
    {
        $this->createTable('{{%proxies}}', [
            'id' => $this->primaryKey()->unsigned(),
            'country' => $this->string(200),
            'country_code' => $this->string(3),
            'google' => $this->tinyInteger(1),
            'ip' => $this->string(50),
            'port' => $this->integer(),
            'time' => $this->float(),
            'type' => $this->tinyInteger(11),
            'createdAt' => $this->integer(),
            'updatedAt' => $this->integer(),
            'checked_at' => $this->integer()->comment('proverrenni server ili net. To est on voobsche bil rabochii srazu posle '),
            'server_up' => $this->tinyInteger(1)->comment('server rabochii ili net'),
            'speed' => $this->string(10),
            'error' => $this->text()->comment('oshibka kotorya generitsya pri zaprose na dostupnost proxy'),
            'good_bad' => $this->integer(),
            'good_good' => $this->integer(),
            'bad_bad' => $this->integer(),
            'bad_good' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
            'source' => $this->string(100),
        ]);

        $this->createIndex('proxies_createdAt_index', '{{%proxies}}', 'createdAt');
        $this->createIndex('proxies_ip_index', '{{%proxies}}', 'ip');
        $this->createIndex('proxies_country_code_index', '{{%proxies}}', 'country_code');
    }

    public function down()
    {
        $this->dropTable('{{%proxies}}');
    }
}
