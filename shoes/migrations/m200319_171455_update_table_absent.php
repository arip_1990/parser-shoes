<?php

use yii\db\Migration;

class m200319_171455_update_table_absent extends Migration
{
    public function up()
    {
        $this->createTable('{{%absent}}', [
            'id' => $this->primaryKey()->unsigned(),
            'image_id' => $this->integer()->unsigned()->notNull()->comment('id kartinok kotorie Serega ne nashel'),
            'count' => $this->integer()->unsigned()->defaultValue('0'),
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'poi' => $this->string(10),
        ]);

        $this->createIndex('unique_index', '{{%absent}}', 'image_id', true);
    }

    public function down()
    {
        $this->dropTable('{{%absent}}');
    }
}
