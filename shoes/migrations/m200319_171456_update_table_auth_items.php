<?php

use yii\db\Migration;

class m200319_171456_update_table_auth_items extends Migration
{
    public function up()
    {
        $this->createTable('{{%auth_items}}', [
            'name' => $this->string(64)->notNull()->append('PRIMARY KEY'),
            'type' => $this->smallInteger()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->binary(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('idx-auth_item-type', '{{%auth_items}}', 'type');
        $this->createIndex('rule_name', '{{%auth_items}}', 'rule_name');
    }

    public function down()
    {
        $this->dropTable('{{%auth_items}}');
    }
}
