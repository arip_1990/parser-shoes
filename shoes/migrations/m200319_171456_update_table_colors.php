<?php

use yii\db\Migration;

class m200319_171456_update_table_colors extends Migration
{
    public function up()
    {
        $this->createTable('{{%colors}}', [
            'id' => $this->primaryKey()->unsigned(),
            'color_text_en' => $this->string(100),
            'color_text_fr' => $this->string(100),
            'color_hex' => $this->string(100),
            'color_rgb' => $this->string(100),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('color_hex_index', '{{%colors}}', 'color_hex');
    }

    public function down()
    {
        $this->dropTable('{{%colors}}');
    }
}
