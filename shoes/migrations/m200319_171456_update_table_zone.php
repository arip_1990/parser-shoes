<?php

use yii\db\Migration;

class m200319_171456_update_table_zone extends Migration
{
    public function up()
    {
        $this->createTable('{{%zone}}', [
            'zone_id' => $this->primaryKey(10),
            'country_code' => $this->char(2)->notNull(),
            'zone_name' => $this->string(35)->notNull(),
        ]);

        $this->createIndex('idx_zone_name', '{{%zone}}', 'zone_name');
        $this->createIndex('idx_country_code', '{{%zone}}', 'country_code');
    }

    public function down()
    {
        $this->dropTable('{{%zone}}');
    }
}
