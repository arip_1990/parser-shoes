<?php

use yii\db\Migration;

class m200319_171456_update_table_shoes extends Migration
{
    public function up()
    {
        $this->createTable('{{%shoes}}', [
            'id' => $this->primaryKey()->unsigned(),
            'product_id' => $this->integer()->unsigned(),
            'brand' => $this->string(50),
            'description' => $this->text(),
            'name' => $this->string(250),
            'color' => $this->string(100),
            'delivery' => $this->text()->comment('Sobschenie o dostavki'),
            'currency' => $this->tinyInteger(1)->comment('1 - EUR, 2 - USD, 3 - pound'),
            'price' => $this->float(),
            'price_old' => $this->float(),
            'reference' => $this->string(50),
            'rating' => $this->string(50),
            'sex' => $this->tinyInteger(4)->comment('0 - men, 1 - women, 3 - garcons, 4 - filles'),
            'upper' => $this->string(50),
            'url' => $this->string(200),
            'fermeture' => $this->string(200),
            'semelle_usure' => $this->string(200),
            'doublure' => $this->string(200)->comment('подкладка'),
            'semelle_de_proprete' => $this->string(200)->comment('iz chego sdelana stelka'),
            'article' => $this->string(50),
            'dessus' => $this->string(200),
            'motif' => $this->string(200),
            'pbid' => $this->integer()->unsigned(),
            'sub_name' => $this->string(100),
            'color_set' => $this->tinyInteger(1)->comment('ustanovlen li dlya nee chvet'),
            'color_not_find' => $this->tinyInteger(1)->comment('voobsche nujno li iskat '),
            'available' => $this->tinyInteger(1)->defaultValue('1'),
            'images_done' => $this->tinyInteger(4)->defaultValue('0'),
            'number_check' => $this->integer()->comment('kolichestvo popitok proverki magazina'),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('image_done_index', '{{%shoes}}', 'images_done');
        $this->createIndex('number_check_index', '{{%shoes}}', 'number_check');
        $this->createIndex('availbale_index', '{{%shoes}}', 'available');
        $this->createIndex('set_color_index', '{{%shoes}}', ['color_set', 'color_not_find']);
        $this->createIndex('updated_at', '{{%shoes}}', 'updated_at');
        $this->createIndex('pbib_index', '{{%shoes}}', 'pbid');
        $this->createIndex('shoes_product_id_price_index', '{{%shoes}}', ['product_id', 'price']);
        $this->createIndex('shoes_created_at_index', '{{%shoes}}', 'created_at');
        $this->createIndex('brand_index', '{{%shoes}}', 'brand');
    }

    public function down()
    {
        $this->dropTable('{{%shoes}}');
    }
}
