<?php

use yii\db\Migration;

class m200319_171456_update_table_color extends Migration
{
    public function up()
    {
        $this->createTable('{{%color}}', [
            'id' => $this->primaryKey()->unsigned(),
            'color' => $this->string(10),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%color}}');
    }
}
