<?php

use yii\db\Migration;

class m200319_171456_update_table_type extends Migration
{
    public function up()
    {
        $this->createTable('{{%type}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(50),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%type}}');
    }
}
