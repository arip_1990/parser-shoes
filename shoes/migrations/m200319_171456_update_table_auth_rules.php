<?php

use yii\db\Migration;

class m200319_171456_update_table_auth_rules extends Migration
{
    public function up()
    {
        $this->createTable('{{%auth_rules}}', [
            'name' => $this->string(64)->notNull()->append('PRIMARY KEY'),
            'data' => $this->binary(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%auth_rules}}');
    }
}
