<?php

use yii\db\Migration;

class m200319_171456_update_table_test extends Migration
{
    public function up()
    {
        $this->createTable('{{%test}}', [
            'id' => $this->primaryKey()->unsigned(),
            'text1' => $this->text(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%test}}');
    }
}
