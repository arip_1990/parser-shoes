<?php

use yii\db\Migration;

class m200319_171456_update_table_colors_copy_ne_ispolzyu extends Migration
{
    public function up()
    {
        $this->createTable('{{%colors_copy_ne_ispolzyu}}', [
            'id' => $this->primaryKey()->unsigned(),
            'color_text' => $this->string(100),
            'color_hex' => $this->string(100),
            'color_rgb' => $this->string(100),
            'updated_at' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%colors_copy_ne_ispolzyu}}');
    }
}
