<?php

use yii\db\Migration;

class m200319_171456_update_table_country_timezone extends Migration
{
    public function up()
    {
        $this->createTable('{{%country_timezone}}', [
            'country_code' => $this->char(2),
            'country_name' => $this->string(45),
        ]);

        $this->createIndex('idx_country_code', '{{%country_timezone}}', 'country_code');
    }

    public function down()
    {
        $this->dropTable('{{%country_timezone}}');
    }
}
