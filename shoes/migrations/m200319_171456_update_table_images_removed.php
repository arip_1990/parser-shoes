<?php

use yii\db\Migration;

class m200319_171456_update_table_images_removed extends Migration
{
    public function up()
    {
        $this->createTable('{{%images_removed}}', [
            'id' => $this->primaryKey(),
            'image_id' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('images_removed_image_id_index', '{{%images_removed}}', 'image_id');
    }

    public function down()
    {
        $this->dropTable('{{%images_removed}}');
    }
}
