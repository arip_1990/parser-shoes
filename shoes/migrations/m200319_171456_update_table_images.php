<?php

use yii\db\Migration;

class m200319_171456_update_table_images extends Migration
{
    public function up()
    {
        $this->createTable('{{%images}}', [
            'id' => $this->primaryKey()->unsigned(),
            'patch' => $this->string(150),
            'real_patch' => $this->string(100),
            'shooes_id' => $this->integer(),
            'product_id' => $this->integer()->unsigned(),
            'get_color' => $this->tinyInteger(1)->comment('bili li vzyati chveta'),
            'indexed' => $this->tinyInteger(1)->comment('1 - esli mojno indeksirovat'),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('ready_index', '{{%images}}', ['indexed', 'id']);
        $this->createIndex('images_patch_index', '{{%images}}', 'patch');
        $this->createIndex('get_color_index', '{{%images}}', 'get_color');
        $this->createIndex('product_index', '{{%images}}', 'product_id');
    }

    public function down()
    {
        $this->dropTable('{{%images}}');
    }
}
