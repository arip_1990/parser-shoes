<?php

use yii\db\Migration;

class m200319_171456_update_table_color_of_product extends Migration
{
    public function up()
    {
        $this->createTable('{{%color_of_product}}', [
            'id' => $this->primaryKey()->unsigned(),
            'product_id' => $this->integer()->unsigned(),
            'color_id' => $this->integer()->unsigned(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('color_id_index', '{{%color_of_product}}', 'color_id');
    }

    public function down()
    {
        $this->dropTable('{{%color_of_product}}');
    }
}
