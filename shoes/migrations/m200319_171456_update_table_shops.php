<?php

use yii\db\Migration;

class m200319_171456_update_table_shops extends Migration
{
    public function up()
    {
        $this->createTable('{{%shops}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(100)->notNull()->defaultValue(''),
            'address' => $this->string(100)->defaultValue(''),
            'svg_logo' => $this->string(100),
            'adult' => $this->tinyInteger(4)->notNull()->defaultValue('0'),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('name_index', '{{%shops}}', 'name');

        // Nke
        $this->insert('shops', [
            'name' => 'nike_men',
            'address' => '11 avenue de Gaulle, 75007 Paris  France',
            'svg_logo' => 'Nike_logo.svg',
            'adult' => '1',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'nike_women',
            'address' => '11 avenue de Gaulle, 75007 Paris  France',
            'svg_logo' => 'Nike_logo.svg',
            'adult' => '1',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'nike_garcons',
            'address' => '11 avenue de Gaulle, 75007 Paris  France',
            'svg_logo' => 'Nike_logo.svg',
            'adult' => '0',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'nike_filles',
            'address' => '11 avenue de Gaulle, 75007 Paris  France',
            'svg_logo' => 'Nike_logo.svg',
            'adult' => '0',
            'created_at' => null,
            'updated_at' => null
        ]);

        // adidas
        $this->insert('shops', [
            'name' => 'adidas_men',
            'address' => '',
            'svg_logo' => 'Adidas_logo.svg',
            'adult' => '1',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'adidas_women',
            'address' => '',
            'svg_logo' => 'Adidas_logo.svg',
            'adult' => '1',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'adidas_garcons',
            'address' => '',
            'svg_logo' => 'Adidas_logo.svg',
            'adult' => '0',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'adidas_filles',
            'address' => '',
            'svg_logo' => 'Adidas_logo.svg',
            'adult' => '0',
            'created_at' => null,
            'updated_at' => null
        ]);

        // Zalando
        $this->insert('shops', [
            'name' => 'zalando_men',
            'address' => '',
            'svg_logo' => 'Zalando_logo.svg',
            'adult' => '1',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'zalando_women',
            'address' => '',
            'svg_logo' => 'Zalando_logo.svg',
            'adult' => '1',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'zalando_garcons',
            'address' => '',
            'svg_logo' => 'Zalando_logo.svg',
            'adult' => '0',
            'created_at' => null,
            'updated_at' => null
        ]);
        $this->insert('shops', [
            'name' => 'zalando_filles',
            'address' => '',
            'svg_logo' => 'Zalando_logo.svg',
            'adult' => '0',
            'created_at' => null,
            'updated_at' => null
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%shops}}');
    }
}
