<?php

use yii\db\Migration;

class m200319_171456_update_table_special extends Migration
{
    public function up()
    {
        $this->createTable('{{%special}}', [
            'id' => $this->primaryKey()->unsigned(),
            'symbol' => $this->string(10),
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%special}}');
    }
}
