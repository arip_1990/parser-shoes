<?php

use yii\db\Migration;

class m200319_171455_update_table_auth_assignments extends Migration
{
    public function up()
    {
        $this->createTable('{{%auth_assignments}}', [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARYKEY', '{{%auth_assignments}}', ['item_name', 'user_id']);
        $this->createIndex('idx-auth_assignments-user_id', '{{%auth_assignments}}', 'user_id');
    }

    public function down()
    {
        $this->dropTable('{{%auth_assignments}}');
    }
}
