<?php

use yii\db\Migration;

class m200319_171456_update_table_ip_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%ip_table}}', [
            'ip_from' => $this->integer(10)->unsigned(),
            'ip_to' => $this->integer(10)->unsigned(),
            'country_code' => $this->char(2),
            'country_name' => $this->string(64),
        ]);

        $this->createIndex('idx_ip_from_to', '{{%ip_table}}', ['ip_from', 'ip_to']);
        $this->createIndex('idx_ip_to', '{{%ip_table}}', 'ip_to');
        $this->createIndex('idx_ip_from', '{{%ip_table}}', 'ip_from');
    }

    public function down()
    {
        $this->dropTable('{{%ip_table}}');
    }
}
