<?php

use yii\db\Migration;

class m200319_171456_update_table_yii_queue extends Migration
{
    public function up()
    {
        $this->createTable('{{%yii_queue}}', [
            'id' => $this->primaryKey(),
            'channel' => $this->string(255)->notNull(),
            'job' => $this->binary()->notNull(),
            'pushed_at' => $this->integer()->notNull(),
            'ttr' => $this->integer()->notNull(),
            'delay' => $this->integer()->notNull()->defaultValue(0),
            'priority' => $this->integer()->unsigned()->notNull()->defaultValue(1024),
            'reserved_at' => $this->integer(),
            'attempt' => $this->integer(),
            'done_at' => $this->integer()
        ]);

        $this->createIndex('channel', '{{%yii_queue}}', 'channel');
        $this->createIndex('priority', '{{%yii_queue}}', 'priority');
        $this->createIndex('reserved_at', '{{%yii_queue}}', 'reserved_at');
    }

    public function down()
    {
        $this->dropTable('{{%yii_queue}}');
    }
}
