<?php

use yii\db\Migration;

class m200319_171456_update_table_process extends Migration
{
    public function up()
    {
        $this->createTable('{{%process}}', [
            'id' => $this->primaryKey()->unsigned(),
            'shop_id' => $this->integer(),
            'status' => $this->tinyInteger(1),
            'scrabble_page' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->createIndex('unique_cle_double', '{{%process}}', ['shop_id', 'status'], true);
    }

    public function down()
    {
        $this->dropTable('{{%process}}');
    }
}
