<?php

namespace console\helpers;

use console\interfaces\ShopInterface;
use console\models\Proxy;
use console\repositories\mailRepositories\sendMail;
use Yii;

/**
 * Class Utils
 * @package console\helpers
 */
class Utils
{
    const LOG_DIR = 'runtime/logs/';

    public static function log(string $str, string $file_name, sendMail $mailer = null): void
    {
        if (!file_exists(self::LOG_DIR))
            mkdir(self::LOG_DIR);

        $log = '[' . date('Y-m-d H:i:s') . '] ' . $str . PHP_EOL;

        if ($mailer) $mailer->sendEmailGeneralError('Error!', $log);

        $path_file = self::LOG_DIR . $file_name . '.log';

        $fo = fopen($path_file, 'a');
        fwrite($fo, $log);
        fclose($fo);
    }

    public static function checkParseOrNot(): bool
    {
        $settings = Yii::$app->db->createCommand('SELECT * FROM settings')->queryOne();
        return $settings['image_parse_on_off'];
    }

    public static function checkDirectoryForListProductCreated(): void
    {
        if (!file_exists(ShopInterface::LIST_SHOP_PRODUCTS) && !is_dir(ShopInterface::LIST_SHOP_PRODUCTS))
            mkdir(ShopInterface::LIST_SHOP_PRODUCTS);
    }

    public static function checkScanList(string $category): void
    {
        $list = self::scanDir(ShopInterface::LIST_SHOP_PRODUCTS);

        if (count($list) && in_array($category, $list))
            throw new \Exception('takoi fail uje sushestvuet: ' . $category);
    }

    public static function scanDir(string $dir): array
    {
        if (!is_dir($dir))
            throw new \Exception('eto ne papka: ' . $dir);

        $list = scandir($dir, 1);
        unset($list[count($list) - 1], $list[count($list) - 1]);

        return $list;
    }

    public static function getProxies(int $bad_max = 3): array
    {
        return Proxy::find()->where(['bad_bad' => null])->orWhere(['<', 'bad_bad', $bad_max])->all();
    }

    public static function getAgents(): array
    {
        return [
            ['User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36']
        ];
    }

    public static function prepareDirectory(string $category): string
    {
        if (!file_exists(ShopInterface::DIR_STATIC))
            mkdir(ShopInterface::DIR_STATIC);

        $dir_shop_name = ShopInterface::DIR_STATIC . DIRECTORY_SEPARATOR . $category;

        if (!file_exists($dir_shop_name))
            mkdir($dir_shop_name);

        $timestamp_dirs = self::scanDir($dir_shop_name);
        if (count($timestamp_dirs)) {
            foreach ($timestamp_dirs as $key => $dir) {
                $list = self::scanDir($dir_shop_name . DIRECTORY_SEPARATOR . $dir);
                if (!count($list)) {
                    self::removeDirectory($dir_shop_name . DIRECTORY_SEPARATOR . $dir);
                    unset($timestamp_dirs[$key]);
                }
            }
        }

        if (count($timestamp_dirs) >= ShopInterface::MAX_NUMBER_PER_DIRECTORY)
            throw new \Exception('max number per directory');

        $name_directory = $dir_shop_name . DIRECTORY_SEPARATOR . time();

        if (file_exists($name_directory) && is_dir($name_directory))
            self::removeDirectory($name_directory);  //__ esli skript zapuschen v odu i tu je sekndu

        mkdir($name_directory);

        return $name_directory;
    }

    public static function saveImages(string $url, string $path, string $name_file, string $post_dir = 'tmp'): bool
    {
        $path .= DIRECTORY_SEPARATOR . $post_dir;
        $content = null;

        foreach (static::getProxies() as $proxy) {
            try {
                $content = self::curlRequest($url, $proxy->ip . ':' . $proxy->port);
                break;
            }
            catch (\Exception $e) {}
        }

        if (!$content)
            return false;

        if (!file_exists($path))
            mkdir($path);

        $path .= DIRECTORY_SEPARATOR . $name_file . '.jpg';

        $fp_image = fopen($path, 'x');
        fwrite($fp_image, $content);
        fclose($fp_image);

        return true;
    }

    public static function removeDirectory(string $dir): void
    {
        if ($objs = glob($dir . "/*"))
            foreach ($objs as $obj) is_dir($obj) ? self::removeDirectory($obj) : unlink($obj);

        rmdir($dir);
    }

    public static function rotation_proxy(Proxy &$proxy, $bad = false): void
    {
        if ($bad) {
            if ($proxy->server_up) {
                $proxy->good_bad++;
                $proxy->server_up = 0;
            }
            else
                $proxy->bad_bad++;
        }
        else {
            if ($proxy->server_up)
                $proxy->good_good++;
            else {
                $proxy->bad_good++;
                $proxy->server_up = 1;
            }
        }
        $proxy->checked_at = time();
    }

    public static function curlRequest(string $url, ?string $proxy = null): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($url));
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_HTTPHEADER, self::getAgents()[0]);

        if ($proxy)
            curl_setopt($ch, CURLOPT_PROXY, 'http://' . $proxy);

        $page = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_RESPONSE_CODE);
        curl_close($ch);

        if ($httpCode != 200 or !$page)
            throw new \Exception('Ne udalos poluchit stranicu');

        return $page;
    }

    public static function printStatus(array $data, ?bool $success = null): void
    {}
}
