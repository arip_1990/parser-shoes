<?php

namespace console\helpers;

class StringHelper {
    public static function randomString(int $length = 15, $special = null, $directory = false): string {
        $validCharacters = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ1234567890";
        $validCharacters_special = "abcdefghijklmnopqrstuxyvwzABCDEFGHIJKLMNOPQRSTUXYVWZ1234567890+-:;";


        $validCharacters = isset($special) ? $validCharacters : $validCharacters_special;
        $validCharacters = (!$directory) ? $validCharacters : 'abcdefghijklmnopqrstuxyvwz';

        $validCharNumber = strlen($validCharacters);

        $result = "";

        //

        for ($i = 0; $i < $length; $i++) {
            $index = mt_rand(0, $validCharNumber - 1);
            $result .= $validCharacters[$index];
        }
        return $result;
    }

    public static function slashEscape(string $str): string {
        return str_replace('"', '\"', str_replace("'", "\'", $str));
    }

    public static function getFileNameWith2Slash(string $url = null, bool $only2Slash = false): string {
        return ($only2Slash)
            ? substr($url, 0, 2) . '/' . substr($url, 2, 2)
            : substr($url, 0, 2) . '/' . substr($url, 2, 2) . '/' . $url;
    }
}

