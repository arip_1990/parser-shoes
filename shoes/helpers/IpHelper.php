<?php

namespace console\helpers;

class IpHelper {
    public static function get_real_ip(): ?string {
        // opredelyaem nastoyaschii ip

        if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])) return $_SERVER['HTTP_CF_CONNECTING_IP'];
        elseif ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') return $_SERVER['REMOTE_ADDR'];
        else return null;
    }

    public static function generateIp(): string {
        return "" . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255) . "." . mt_rand(0, 255);
    }
}