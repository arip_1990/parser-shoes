<?php

namespace console\helpers;

use console\models\Exceptions;

class ExceptionsLog {
    private $queue;

    public function __construct() {
        $this->queue = new Exceptions();
    }

    public function insert(int $status, string $shop_name, string $cause, int $code_error): void {
        $this->queue->status = $status;
        $this->queue->name_shop = $shop_name;
        $this->queue->cause = $cause;
        $this->queue->code_error = $code_error;
        $this->queue->save(false);
    }
}