<?php

namespace console\jobs;

use console\models\Image;
use console\models\Product;
use console\models\Shoes;
use console\models\Size;
use console\extended\checkDirectory;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class WriteJob extends BaseObject implements JobInterface {
    public $category, $sql;

    public function execute($queue) {
        $this->sql = json_decode($this->sql);
        $transaction = \Yii::$app->db->beginTransaction();

        try {
            if ($this->sql->update) {
                $shoes = Shoes::find()->where(['id' => $this->sql->id])->one();
                $shoes->price = $this->sql->price;
                $shoes->updated_at = $this->sql->timeStamp;
                $shoes->save();

                $product_id = $shoes->product_id;
                unset($shoes);

                Size::deleteAll(['product_id', $product_id]);
                $this->saveSizes($product_id);
            }
            else {
                $product = new Product();
                $product->shop_id = $this->sql->product->shop_id;
                $product->type_id = $this->sql->product->type_id;
                $product->created_at = $this->sql->product->timeStamp;
                $product->updated_at = $this->sql->product->timeStamp;
                $product->save();

                $product_id = $product->id;
                unset($product);

                $shoes = new Shoes();
                $shoes->product_id = $product_id;
                $shoes->article = $this->sql->shoes->article;
                $shoes->brand = $this->sql->shoes->brand;
                $shoes->name = $this->sql->shoes->name;
                $shoes->color = $this->sql->shoes->color;
                $shoes->price = $this->sql->shoes->price;
                $shoes->currency = $this->sql->shoes->currency;
                $shoes->url = $this->sql->shoes->url;
                $shoes->sex = $this->sql->shoes->sex;
                $shoes->dessus = $this->sql->shoes->dessus;
                $shoes->doublure = $this->sql->shoes->doublure;
                $shoes->semelle_de_proprete = $this->sql->shoes->semelle_de_proprete;
                $shoes->semelle_usure = $this->sql->shoes->semelle_usure;
                $shoes->fermeture = $this->sql->shoes->fermeture;
                $shoes->reference = $this->sql->shoes->reference;
                $shoes->created_at = $this->sql->shoes->timeStamp;
                $shoes->updated_at = $this->sql->shoes->timeStamp;
                $shoes->save();

                $shoes_id = $shoes->id;
                $article = $shoes->article ?: $shoes->reference;
                unset($shoes);

                $this->saveSizes($product_id);
                $this->saveImages($product_id, $shoes_id);

                (new checkDirectory())->init($this->category, $article);
            }

            $transaction->commit();
        }
        catch (\Exception $e) {
            $transaction->rollBack();
        }
    }

    private function saveSizes(int $product_id): void
    {
        foreach ($this->sql->size->sizes as $item) {
            $size = new Size();
            $size->product_id = $product_id;
            $size->size = $item;
            $size->created_at = $this->sql->size->timeStamp;
            $size->updated_at = $this->sql->size->timeStamp;
            $size->save();
        }
    }

    private function saveImages(int $product_id, int $shoes_id): void
    {
        foreach ($this->sql->images->patch as $item) {
            $image = new Image();
            $image->product_id = $product_id;
            $image->shooes_id = $shoes_id;
            $image->patch = $item;
            $image->created_at = $this->sql->images->timeStamp;
            $image->updated_at = $this->sql->images->timeStamp;
            $image->save();
        }
    }
}