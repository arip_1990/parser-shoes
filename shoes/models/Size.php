<?php

namespace console\models;

use yii\db\ActiveRecord;

class Size extends ActiveRecord {

    public static function tableName() {
        return '{{%size}}';
    }
}