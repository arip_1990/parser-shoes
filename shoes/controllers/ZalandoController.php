<?php

namespace console\controllers;

use console\interfaces\ShopInterface;
use console\repositories\shopRepositories\ShopReadRepository;
use console\services\ParsingService;
use console\extended\checkDirectory;
use console\extended\MultiParser;
use console\helpers\Utils;
use console\jobs\WriteJob;
use console\repositories\mailRepositories\sendMail;
use console\shops\GeneralShopClass;
use console\shops\Zalando;
use Yii;
use yii\base\Action;
use yii\console\Controller;

/**
 * Class ZalandoController
 * @package console\controllers
 * @property  yii\db\Connection $connection
 */
class ZalandoController extends Controller
{
    const NAME_MEN = 'zalando_men';
    const NAME_WOMEN = 'zalando_women';
    const NAME_GARCONES = 'zalando_garcons';
    const NAME_FILLES = 'zalando_filles';

    private $checkDirectory;
    private $mailer;

    //___Meta info for file
    private $start_product_id;
    private $shopReadRepositories;
    private $parsingService;
    private $_name_directory = ''; //___ 158790909  //  1 raz prisvaivaem pri pervom sozdanii irarxii papok

    /**
     * ZalandoController constructor.
     * @param $id
     * @param $module
     * @param sendMail $mailer
     * @param checkDirectory $checkDirectory
     * @param ShopReadRepository $shopReadRepository
     * @param ParsingService $parsingService
     * @param array $config
     * @throws \yii\db\Exception
     */
    public function __construct($id, $module,
                                sendMail $mailer,
                                checkDirectory $checkDirectory,
                                ShopReadRepository $shopReadRepository,
                                ParsingService $parsingService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->parsingService = $parsingService;
        $this->checkDirectory = $checkDirectory;
        $this->shopReadRepositories = $shopReadRepository;
        $this->mailer = $mailer;

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 18000;')->execute();
        Yii::$app->db->createCommand('SET SESSION interactive_timeout = 18000;')->execute();
    }

    public function beforeAction($action): bool
    {
//        if (!Utils::checkParseOrNot())
//            return false;
        return parent::beforeAction($action);
    }

    public function actionInsert(): void
    {
        Yii::$app->queue->run(false);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListMen()
    {
        $this->scanForList(
            'https://www.zalando.fr/chaussures-homme/?p=',
            self::NAME_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListWomen()
    {
        $this->scanForList(
            'https://www.zalando.fr/chaussures-femme/?p=',
            self::NAME_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListGarcons()
    {
        $this->scanForList(
            'https://www.zalando.fr/chaussures-enfant/?gender=17&p=',
            self::NAME_GARCONES);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListFilles()
    {
        $this->scanForList(
            'https://www.zalando.fr/chaussures-enfant/?gender=18&p=',
            self::NAME_FILLES);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageMen()
    {
        $this->parsePage(self::NAME_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageWomen()
    {
        $this->parsePage(self::NAME_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageGarcons()
    {
        $this->parsePage(self::NAME_GARCONES);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageFilles()
    {
        $this->parsePage(self::NAME_FILLES);
    }

    private function scanForList(string $url, string $category): void
    {
        $zalando = new Zalando($this->mailer);
        $zalando->console_web = GeneralShopClass::LAUNCH_WEB;
        $zalando->url = $url;
        $zalando->category = $category;

        try {
            $this->parsingService->scanForList($zalando, Utils::getProxies());

            Utils::log('>>> Parsing silok zakoncheno <<<', 'zalando');
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine(), 'zalando');
        }
        finally {
            if (count($zalando->data)) {
                $fo = fopen(ShopInterface::LIST_SHOP_PRODUCTS . DIRECTORY_SEPARATOR . $category, 'w');
                foreach (array_unique($zalando->data) as $link)
                    fwrite($fo, $link . PHP_EOL);
                fclose($fo);
            }
            foreach ($zalando->proxies as $proxy)
                $proxy->save();
        }
    }

    private function parsePage(string $category): void
    {
        $zalando = new Zalando($this->mailer);
        $zalando->category = $category;

        try {
            $this->parsingService->parseProduct($zalando, Utils::getProxies());

            Utils::log('>>> Parsing stranic zakoncheno <<<', 'zalando');
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine(), 'zalando');
        }
        finally {
            foreach ($zalando->data as $sql)
                Yii::$app->queue->push(new WriteJob(['category' => $category, 'sql' => $sql]));

            foreach ($zalando->proxies as $proxy)
                $proxy->save();
        }
    }
}