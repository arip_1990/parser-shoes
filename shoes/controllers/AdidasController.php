<?php

namespace console\controllers;

use console\interfaces\ShopInterface;
use console\services\ParsingService;
use console\extended\checkDirectory;
use console\extended\MultiParser;
use console\helpers\Utils;
use console\jobs\WriteJob;
use console\repositories\mailRepositories\sendMail;
use console\shops\Adidas;
use console\shops\GeneralShopClass;
use Yii;
use yii\base\Action;
use yii\console\Controller;

/**
 * Class AdidasController
 * @package console\controllers
 * @property  \yii\db\Connection $connection
 */
class AdidasController extends Controller //____ berem vse dannie na Nike i vstavlyaem v BD
{
    const NAME_ADIDAS_MEN = 'adidas_men';
    const NAME_ADIDAS_WOMEN = 'adidas_women';
    const NAME_ADIDAS_GARCONS = 'adidas_garcons';
    const NAME_ADIDAS_FILLES = 'adidas_filles';

    private $checkDirectory;
    private $mailer;

    private $tableau_sex_ass;
    private $parsingService;


    /**
     * NikeController constructor
     * @param string $id
     * @param $module
     * @param sendMail $mailer
     * @param checkDirectory $checkDirectory
     * @param ParsingService $parsingService
     * @param array $config
     */
    public function __construct(string $id, $module,
                                sendMail $mailer,
                                checkDirectory $checkDirectory,
                                ParsingService $parsingService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->checkDirectory = $checkDirectory;
        $this->parsingService = $parsingService;
        $this->mailer = $mailer;

        $this->tableau_sex_ass = [
            self::NAME_ADIDAS_MEN => 0,
            self::NAME_ADIDAS_WOMEN => 1,
            self::NAME_ADIDAS_GARCONS => 2,
            self::NAME_ADIDAS_FILLES => 3
        ];

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 18000;')->execute();
        Yii::$app->db->createCommand('SET SESSION interactive_timeout = 18000;')->execute();
    }

    /**
     * @param Action $action
     * @return bool
     * @throws \Exception
     */
    public function beforeAction($action)
    {
//        if (!Utils::checkParseOrNot())
//            return false;
        return parent::beforeAction($action);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListMen()
    {
        $this->scanForList(
            'https://www.adidas.fr/chaussures-hommes?start=',
            self::NAME_ADIDAS_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListWomen()
    {
        $this->scanForList(
            'https://www.adidas.fr/chaussures-femmes?start=',
            self::NAME_ADIDAS_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListGarcons()
    {
        $this->scanForList(
            'https://www.adidas.fr/garcons-adolescents_8_16_ans?start=',
            self::NAME_ADIDAS_GARCONS);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListFilles()
    {
        $this->scanForList(
            'https://www.adidas.fr/filles-adolescents_8_16_ans?start=',
            self::NAME_ADIDAS_FILLES);
    }


    /**
     * @throws \Exception
     */
    public function actionParsePageMen()
    {
        $this->parsePage(self::NAME_ADIDAS_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageWomen()
    {
        $this->parsePage(self::NAME_ADIDAS_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageGarcons()
    {
        $this->parsePage(self::NAME_ADIDAS_GARCONS);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageFilles()
    {
        $this->parsePage(self::NAME_ADIDAS_FILLES);
    }

    private function scanForList(string $url, string $category): void
    {
        $adidas = new Adidas($this->mailer);
        $adidas->console_web = GeneralShopClass::LAUNCH_WEB;
        $adidas->url = $url;
        $adidas->category = $category;

        try {
            $this->parsingService->scanForList($adidas, Utils::getProxies());

            Utils::log('>>> Parsing silok zakoncheno <<<', 'adidas');
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine(), 'adidas');
        }
        finally {
            if (count($adidas->data)) {
                $fo = fopen(ShopInterface::LIST_SHOP_PRODUCTS . DIRECTORY_SEPARATOR . $category, 'w');
                foreach (array_unique($adidas->data) as $link)
                    fwrite($fo, $link . PHP_EOL);
                fclose($fo);
            }
            foreach ($adidas->proxies as $proxy)
                $proxy->save();
        }
    }

    private function parsePage(string $category): void
    {
        $adidas = new Adidas($this->mailer);
        $adidas->category = $category;

        try {
            $this->parsingService->parseProduct($adidas, Utils::getProxies());

            Utils::log('>>> Parsing stranic zakoncheno <<<', 'adidas');
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine(), 'adidas');
        }
        finally {
            foreach ($adidas->data as $sql)
                Yii::$app->queue->push(new WriteJob(['category' => $category, 'sql' => $sql]));

            foreach ($adidas->proxies as $proxy)
                $proxy->save();
        }
    }
}