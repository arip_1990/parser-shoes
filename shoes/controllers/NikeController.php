<?php

namespace console\controllers;

use console\interfaces\ShopInterface;
use console\repositories\shopRepositories\ShopReadRepository;
use console\services\ParsingService;
use console\extended\checkDirectory;
use console\extended\MultiParser;
use console\helpers\Utils;
use console\jobs\WriteJob;
use console\repositories\mailRepositories\sendMail;
use console\shops\GeneralShopClass;
use console\shops\Nike;
use Yii;
use yii\base\Action;
use yii\console\Controller;

/**
 * Class NikeController
 * @package console\controllers
 * @property  yii\db\Connection $connection
 */
class NikeController extends Controller
{
    const NAME_NIKE_MEN = 'nike_men';
    const NAME_NIKE_WOMEN = 'nike_women';
    const NAME_NIKE_GARCONES = 'nike_garcons';
    const NAME_NIKE_FILLES = 'nike_filles';

    private $checkDirectory;
    private $mailer;
    private $shopReadRepositories;
    private $parsingService;

    /**
     * NikeController constructor.
     * @param string $id
     * @param $module
     * @param sendMail $mailer
     * @param checkDirectory $checkDirectory
     * @param ShopReadRepository $shopReadRepository
     * @param ParsingService $parsingService
     * @param array $config
     */
    public function __construct(string $id, $module,
                                sendMail $mailer,
                                checkDirectory $checkDirectory,
                                ShopReadRepository $shopReadRepository,
                                ParsingService $parsingService,
                                $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->checkDirectory = $checkDirectory;
        $this->shopReadRepositories = $shopReadRepository;
        $this->parsingService = $parsingService;
        $this->mailer = $mailer;

        Yii::$app->db->createCommand('SET SESSION wait_timeout = 18000;')->execute();
        Yii::$app->db->createCommand('SET SESSION interactive_timeout = 18000;')->execute();
    }

    /**
     * @param Action $action
     * @return bool
     * @throws \Exception
     */
    public function beforeAction($action) {
//        if (!Utils::checkParseOrNot())
//            return false;
        return parent::beforeAction($action);
    }

    public function actionInsert()
    {
        Yii::$app->queue->run(false);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListMen()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=homme-chaussures/7puZoi3&pn=',
            self::NAME_NIKE_MEN);
    }


    /**
     * @throws \Exception
     */
    public function actionMakeListWomen()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=femme-chaussures/7ptZoi3&pn=',
            self::NAME_NIKE_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListGarcons()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=garçon-chaussures/7pvZoi3&pn=',
            self::NAME_NIKE_GARCONES);
    }

    /**
     * @throws \Exception
     */
    public function actionMakeListFilles()
    {
        $this->scanForList(
            'https://store.nike.com/html-services/gridwallData?country=FR&lang_locale=fr_FR&gridwallPath=girls-shoes7pwZoi3&pn=',
            self::NAME_NIKE_FILLES);
    }


    /**
     * @throws \Exception
     */
    public function actionParsePageMen()
    {
        $this->parsePage(self::NAME_NIKE_MEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageWomen()
    {
        $this->parsePage(self::NAME_NIKE_WOMEN);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageGarcons()
    {
        $this->parsePage(self::NAME_NIKE_GARCONES);
    }

    /**
     * @throws \Exception
     */
    public function actionParsePageFilles()
    {
        $this->parsePage(self::NAME_NIKE_FILLES);
    }

    private function scanForList(string $url, string $category): void
    {
        $nike = new Nike($this->mailer);
        $nike->console_web = GeneralShopClass::LAUNCH_WEB;
        $nike->url = $url;
        $nike->category = $category;

        try {
            $this->parsingService->scanForList($nike, Utils::getProxies());

            Utils::log('>>> Parsing silok zakoncheno <<<', 'nike');
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine(), 'nike');
        }
        finally {
            if (count($nike->data)) {
                $fo = fopen(ShopInterface::LIST_SHOP_PRODUCTS . DIRECTORY_SEPARATOR . $category, 'w');
                foreach (array_unique($nike->data) as $link)
                    fwrite($fo, $link . PHP_EOL);
                fclose($fo);
            }
            foreach ($nike->proxies as $proxy)
                $proxy->save();
        }
    }

    private function parsePage(string $category): void
    {
        $nike = new Nike($this->mailer);
        $nike->category = $category;

        try {
            $this->parsingService->parseProduct($nike, Utils::getProxies());

            Utils::log('>>> Parsing stranic zakoncheno <<<', 'nike');
        }
        catch (\Exception $e) {
            Utils::log($e->getMessage() . PHP_EOL . $e->getFile() . ' -> ' . $e->getLine(), 'nike');
        }
        finally {
            foreach ($nike->data as $sql)
                Yii::$app->queue->push(new WriteJob(['category' => $category, 'sql' => $sql]));

            foreach ($nike->proxies as $proxy)
                $proxy->save();
        }
    }

}